program format_ppm_image

  implicit none
  
  integer :: i,j,nx,ny,ncols,rd,gn,bl,narg
  character(len=100) :: fname
  
  narg = iargc()
  
  if(narg .ne. 1)then
     write(*,*) 'Wrong number of arguments'
     stop
  endif
    
  call getarg(1,fname)
  
  open(1,file=trim(fname),status='old',form='formatted')
  read(1,*)
  read(1,*)
  read(1,*) nx,ny
  read(1,*) ncols
  
  open(2,file=trim(fname)//'.js',form='formatted')
  do j = 1,ny
      do i = 1,nx
          read(1,*) rd
          read(1,*) gn
          read(1,*) bl
          ! index i+1 comes from i-1 + 2 (+2 is the nghost offset)
!           write(2,'(a,i3,a,i3,a,es15.7,a)') 'dens_old[',i+1,'][',j+1,'] = d1 + d2 * ',(real(rd)+real(gn)+real(bl))/765.0,';'
          write(2,'(a,i3,a,i3,a,f8.6,a)') '        image[',i-1,'][',j-1,'] = ',(real(rd)+real(gn)+real(bl))/765.0,';'
      enddo
  enddo
  close(1)
  close(2)

  stop

end program format_ppm_image
