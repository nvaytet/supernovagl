// // Get window dimensions
// // var e     = document.documentElement;
// var g     = document.getElementsByTagName('body')[0];
// // var scalingx = 0.99;
// // var scalingy = 0.975;
// var scalingx = 1.0;
// var scalingy = 1.0;
// var windx0 = window.innerWidth || e.clientWidth || g.clientWidth;
// var windy0 = window.innerHeight|| e.clientHeight|| g.clientHeight;
// // alert(String(windx0)+" "+String(windy0))
// var windx = scalingx * windx0;
// var windy = scalingy * windy0 - 19;
// var ratio = windy / windx

var windx = 1600;
var windy = 900;
var ratio = windy / windx

// Declare variables
var nvar = 4;
var ndim = 2;
var nx = 256;
var ny = Math.floor(nx * ratio);
var nghosts = 2;
var nnx = nx + 2*nghosts;
var nny = ny + 2*nghosts;
var nx1 = nghosts + 1;
var nx2 = nghosts + nx;
var ny1 = nghosts + 1;
var ny2 = nghosts + ny;
var time = 0.0;
var dt;
var it = 0;
var gam = 1.4;
var gm1 = gam - 1.0;
var time_limit = 10.0;
var cfl = 0.4;
var ncolors = 255;
var large_number = 1.00e+30;
var nout = 1;
var dmin       = 0.2;
var dmax       = 1.5;
var dtnew;
var dfix = 0.1;
var pfix = 1.0e-10;
var icell = 1;
var jcell = 1;

// var soundspeed;
// var wsx;
// var wsy;
// var dtx;
// var dty;
// var kinetic_energy;
// var internal_energy;


// Arrays

var dens_old = new Array(nnx);
for (var i = 0; i < nnx; i++) {
  dens_old[i] = new Array(nny);
}

var dens_new = new Array(nnx);
for (var i = 0; i < nnx; i++) {
  dens_new[i] = new Array(nny);
}

var momx_old = new Array(nnx);
for (var i = 0; i < nnx; i++) {
  momx_old[i] = new Array(nny);
}

var momx_new = new Array(nnx);
for (var i = 0; i < nnx; i++) {
  momx_new[i] = new Array(nny);
}

var momy_old = new Array(nnx);
for (var i = 0; i < nnx; i++) {
  momy_old[i] = new Array(nny);
}

var momy_new = new Array(nnx);
for (var i = 0; i < nnx; i++) {
  momy_new[i] = new Array(nny);
}

var ener_old = new Array(nnx);
for (var i = 0; i < nnx; i++) {
  ener_old[i] = new Array(nny);
}

var ener_new = new Array(nnx);
for (var i = 0; i < nnx; i++) {
  ener_new[i] = new Array(nny);
}

var update_count = new Array(nnx);
for (var i = 0; i < nnx; i++) {
  update_count[i] = new Array(nny);
}

var red   = new Array(ncolors);
var green = new Array(ncolors);
var blue  = new Array(ncolors);

var varxl = new Array(nvar);
var varxr = new Array(nvar);
var varyl = new Array(nvar);
var varyr = new Array(nvar);
var fluxx = new Array(nvar);
var fluxy = new Array(nvar);

// Gaussian kernel ######################################
var kw = 20;
var ngk = 2*kw + 1;
var gkernel = new Array(ngk);
for (var i = 0; i < ngk; i++) {
  gkernel[i] = new Array(ngk);
}
var x = 0.0;
var gsum = 0.0;
var x0 = (ngk+1)*0.5;
var y  = x0*x0/(16.0*Math.log(2.0));
for (var i = 0; i < ngk; i++) {
    for (var j = 0; j < ngk; j++) {
        x = ((i+1)-x0)*((i+1)-x0) + ((j+1)-x0)*((j+1)-x0);
        gkernel[i][j] = Math.exp(-x/y);
        gsum = gsum + gkernel[i][j];
    }
}
for (var i = 0; i < ngk; i++) {
    for (var j = 0; j < ngk; j++) {
       gkernel[i][j] = gkernel[i][j]/gsum;
    }
}


//###############################################################################
// Generate canvas
//###############################################################################

// Determine cell sizes in pixels
var celldx = windx / nx;
var celldy = windy / ny;
var cellid;

document.write("<canvas id=\"myCanvas\" width=\""+windx+"\" height=\""+windy+"\" style=\"border:0px;margin:0px;padding:0px;\">Your browser does not support the canvas element.</canvas>");

// document.write("<svg width=\""+windx+"\" height=\""+windy+"\">");
// for (var i = 0; i < nx; i++) {
//    for (var j = 0; j < ny; j++) {
//        x = i*celldx;
//        y = j*celldy;
//        cellid = "celli"+String(i+nghosts)+"j"+String(j+nghosts);       
//        document.write("<rect id=\""+cellid+"\" x=\""+x+"\" y=\""+y+"\" width=\""+celldx+"\" height=\""+celldy+"\" />");
//    }
// }
// document.write("</svg>");








//###############################################################################
// Generate colormap
//###############################################################################

var gradient = 0.0103;
var offset = 0.0318;
for (var i = 0; i < ncolors; i++) {
    red[i]   = Math.min(Math.max(Math.floor(ncolors*(gradient*(i-1) + offset    )),0),ncolors)
    green[i] = Math.min(Math.max(Math.floor(ncolors*(gradient*(i-1) + offset-1.0)),0),ncolors)
    blue[i]  = Math.min(Math.max(Math.floor(ncolors*(gradient*(i-1) + offset-2.0)),0),ncolors)
}


//###############################################################################
// Simulation setup
//###############################################################################
             
var d1 =  1.0;
var p1 =  1.0;
var p2 = 500.0;
  
for (var i = 0; i < nnx; i++) {
    for (var j = 0; j < nny; j++) {
        
        // Conservative variables
        dens_old[i][j] = d1;
        momx_old[i][j] = 0.0;
        momy_old[i][j] = 0.0;
        ener_old[i][j] = p1 / gm1;
     
//         dist = Math.sqrt((i-0.5*nnx)*(i-0.5*nnx)+(j-0.5*nny)*(j-0.5*nny));
//         if (dist < 0.05*nnx) {
//            ener_old[i][j] = p2 / gm1;
//            dens_old[i][j] = 2.0*d1;
//            alert(String(i)+String(j)+String(dens_old[i][j]))
//         } else {
//            ener_old[i][j] = p1 / gm1;
//         }
        
        dens_new[i][j] = dens_old[i][j];
        momx_new[i][j] = momx_old[i][j];
        momy_new[i][j] = momy_old[i][j];
        ener_new[i][j] = ener_old[i][j];
        
        update_count[i][j] = 0;
    }
}

update_screen();



var theCanvas = document.getElementById('myCanvas');
theCanvas.addEventListener("click", getClickPosition, false);
function getClickPosition(e) {
    var rect = theCanvas.getBoundingClientRect();
//       x: evt.clientX - rect.left,
//       y: evt.clientY - rect.top
//     var xPosition = Math.round((e.clientX-rect.left)/(rect.right -rect.left)*theCanvas.width )/celldx;
//     var yPosition = Math.round((e.clientY-rect.top )/(rect.bottom-rect.top )*theCanvas.height)/celldy;
    icell = Math.round(e.clientX/celldx);
    jcell = Math.round(e.clientY/celldy);
    
//     alert(String(icell)+" "+String(jcell));
    
    run_simulation();
//     
//     var xPosition = e.clientX - rect.left;
//     var yPosition = e.clientY - rect.top;
//     alert(String(xPosition)+" "+String(yPosition)+" "+String(theCanvas.width)+" "+String(theCanvas.height));
//     if (xPosition >= previewxmin && xPosition <= previewxmax) {
//         activate_transitions();
//         var selectImage = parseInt((xPosition-previewxmin)/previewElementSixeX);
//         posx = -imagewidth*selectImage + hidepanelwidth;
//         icap = selectImage;
//         scroll();
//     }
};






function run_simulation() {

time = 0.0
it = 0;


// var dx = 1
var i1 = Math.max(icell-kw,nx1);
var i2 = Math.min(icell+kw,nx2);
var j1 = Math.max(jcell-kw,ny1);
var j2 = Math.min(jcell+kw,ny2);

for (var i = i1; i < i2+1; i++) {
    for (var j = j1; j < j2+1; j++) {
        
        ener_old[i][j] = ener_old[i][j] + p2 * gkernel[i-icell+kw][j-jcell+kw] / gm1;
        ener_new[i][j] = ener_old[i][j];
    }
}

// update_screen();

// alert("here01");

compute_timestep();

while (time < time_limit) {
    
    it = it + 1;
    
//     alert("here02 "+String(time));

    
//     compute_timestep();
  
    compute_hydro();
     
    time = time + dt;
    
//     update_screen();
    
//     if (it % nout == 0) {
//         setTimeout(function(){
//             update_screen();
//         },1000);
//             alert('hit me');
//     }
    
//     alert("here03 "+String(dt)+" "+String(dtnew)+" "+String(it));
    
    dt = dtnew;
     
}

update_screen();






}; // end run simulation



/*
window.setInterval(time_loop, 10);


function time_loop() {

    // Start time loop
    if (time > time_limit) {
        return;
    }

// while (time < time_limit) {
    
    it = it + 1;
    
    compute_timestep();
  
    compute_hydro();
     
    time = time + dt;
     
    update_screen();
    
//     if (it % nout == 0) {
//         setTimeout(function(){
//             update_screen();
//         },0);
//     }
     
};*/

//###############################################################################

function compute_timestep() {


    var soundspeed;
    var wsx;
    var wsy;
    var dtx;
    var dty;
    var kinetic_energy;
    var internal_energy;
    
    dt = large_number;
    

    for (var i = nx1-1; i < nx2; i++) {
        for (var j = ny1-1; j < ny2; j++) {
            
//             dens_old[i][j] = dens_new[i][j];
//             momx_old[i][j] = momx_new[i][j];
//             momy_old[i][j] = momy_new[i][j];
//             ener_old[i][j] = ener_new[i][j];
            
            // Compute sound speed inside cell
            kinetic_energy = 0.5 * (momx_new[i][j]*momx_new[i][j] + momy_new[i][j]*momy_new[i][j]) / dens_new[i][j];
            
            internal_energy = ener_new[i][j] - kinetic_energy;
         
            soundspeed = Math.sqrt( gam * gm1 * internal_energy / dens_new[i][j] );
         
            wsx = Math.abs(momx_new[i][j])/dens_new[i][j] + soundspeed;
            wsy = Math.abs(momy_new[i][j])/dens_new[i][j] + soundspeed;
            wsy = Math.abs(momy_new[i][j])/dens_new[i][j] + soundspeed;
            wsy = Math.abs(momy_new[i][j])/dens_new[i][j] + soundspeed;
            wsy = Math.abs(momy_new[i][j])/dens_new[i][j] + soundspeed;
            
            dtx = 1.0 / wsx;
            dty = 1.0 / wsy;
         
            dt = Math.min(dt,dtx,dty);
            
        }
    }
          
  // CFL condition
  dt = cfl * dt
  
};

function dt_cell(ii,jj) {


    /*dt = large_number;

    for (var i = nx1-1; i < nx2; i++) {
        for (var j = ny1-1; j < ny2; j++) {
            
            dens_old[i][j] = dens_new[i][j];
            momx_old[i][j] = momx_new[i][j];
            momy_old[i][j] = momy_new[i][j];
            ener_old[i][j] = ener_new[i][j];
    */        
    
//         alert("momnew "+String(ii)+" "+String(jj)+" "+String(dens_new[ii][jj])+" "+String(momx_new[ii][jj])+" "+String(momy_new[ii][jj])+" "+String(ener_new[ii][jj])+" ");
    
//     if(dens_new[ii][jj] == 0.0) {
//         alert("neg dens "+String(ii)+" "+String(jj))
//     }
    
    
    
    // Pressure fix
    var density = dens_new[ii][jj]
    var kinetic_energy = 0.5 * (momx_new[ii][jj]*momx_new[ii][jj] + momy_new[ii][jj]*momy_new[ii][jj]) / dens_new[ii][jj];
    var internal_energy = ener_new[ii][jj] - kinetic_energy;

  
  if(density < dfix){
//      write(*,*) d,dcav,' negative DENSITY',rank!,origin
//      ! maintain the same velocity and pressure in cell whilst resetting density
     dens_new[ii][jj] = dfix
     momx_new[ii][jj] = momx_new[ii][jj] / density * dens_new[ii][jj]
     momy_new[ii][jj] = momy_new[ii][jj] / density * dens_new[ii][jj]
     kinetic_energy = 0.5 * (momx_new[ii][jj]*momx_new[ii][jj] + momy_new[ii][jj]*momy_new[ii][jj]) / dens_new[ii][jj];
  }
  
//   ! kinetic energy
//   ke = half*(ucons(2)**2+ucons(3)**2+ucons(4)**2)/ucons(1)
  
//   ! internal energy
  if(internal_energy < pfix*kinetic_energy){
//      write(*,*) p,ke,cavlim*ke,' negative PRESSURE',rank!,origin
     internal_energy = pfix * kinetic_energy
  }
  
  ener_new[ii][jj] = internal_energy + kinetic_energy
  
    
    
    
        // Compute sound speed inside cell
//         var kinetic_energy = 0.5 * (momx_new[ii][jj]*momx_new[ii][jj] + momy_new[ii][jj]*momy_new[ii][jj]) / dens_new[ii][jj];
        
//         var internal_energy = ener_new[ii][jj] - kinetic_energy;
     
        var soundspeed = Math.sqrt( gam * gm1 * internal_energy / dens_new[ii][jj] );
     
        var wsx = Math.abs(momx_new[ii][jj])/dens_new[ii][jj] + soundspeed;
        var wsy = Math.abs(momy_new[ii][jj])/dens_new[ii][jj] + soundspeed;
        
        var dtx = 1.0 / wsx;
        var dty = 1.0 / wsy;
     
        var dtc = Math.min(dtx,dty);
            
//     }
        
//   console.log(ii,jj,dens_new[ii][jj],momx_new[ii][jj],momy_new[ii][jj],ener_new[ii][jj],dtx,dty,dtc)
//   console.log(ii,jj,dtx,dty,dtc,kinetic_energy,internal_energy,soundspeed)
  
          
  // CFL condition
  dtc = cfl * dtc
  
//   alert("momnew "+String(ii)+" "+String(jj)+" "+String(dens_new[ii][jj])+" "+String(momx_new[ii][jj])+" "+String(momy_new[ii][jj])+" "+String(ener_new[ii][jj])+" "+String(dtc));
  
//   alert(String(dtc));
  
  return dtc;
  
};

//###############################################################################

function compute_hydro() {
    
    dtnew = large_number;
//     alert("II"+String(dt_new)+" "+String(large_number))
    
    // fill ghosts cells
    for (var j = ny1-1; j < ny2; j++) {
        for (var i = 0; i < nghosts; i++) {
            dens_old[i][j] = dens_old[nx1-1][j];
            momx_old[i][j] = momx_old[nx1-1][j];
            momy_old[i][j] = momy_old[nx1-1][j];
            ener_old[i][j] = ener_old[nx1-1][j];
        }
        for (var i = nx2; i < nnx; i++) {
            dens_old[i][j] = dens_old[nx2-1][j];
            momx_old[i][j] = momx_old[nx2-1][j];
            momy_old[i][j] = momy_old[nx2-1][j];
            ener_old[i][j] = ener_old[nx2-1][j];
        }
    }
    
    for (var i = nx1-1; i < nx2; i++) {
        for (var j = 0; j < nghosts; j++) {
            dens_old[i][j] = dens_old[i][ny1-1];
            momx_old[i][j] = momx_old[i][ny1-1];
            momy_old[i][j] = momy_old[i][ny1-1];
            ener_old[i][j] = ener_old[i][ny1-1];
        }
        for (var j = ny2; j < nny; j++) {
            dens_old[i][j] = dens_old[i][ny2-1];
            momx_old[i][j] = momx_old[i][ny2-1];
            momy_old[i][j] = momy_old[i][ny2-1];
            ener_old[i][j] = ener_old[i][ny2-1];
        }
    }
    /*
    for (var i = 0; i < nghosts; i++) {
        for (var j = 0; j < nghosts; j++) {
            dens_old[i][j] = dens_old[nx1-1][ny1-1];
            momx_old[i][j] = momx_old[nx1-1][ny1-1];
            momy_old[i][j] = momy_old[nx1-1][ny1-1];
            ener_old[i][j] = ener_old[nx1-1][ny1-1];
        }
    }
    for (var i = nx2; i < nnx; i++) {
        for (var j = 0; j < nghosts; j++) {
            dens_old[i][j] = dens_old[nx2-1][ny1-1];
            momx_old[i][j] = momx_old[nx2-1][ny1-1];
            momy_old[i][j] = momy_old[nx2-1][ny1-1];
            ener_old[i][j] = ener_old[nx2-1][ny1-1];
        }
    }
    for (var i = 0; i < nghosts; i++) {
        for (var j = ny2; j < nny; j++) {
            dens_old[i][j] = dens_old[nx1-1][ny2-1];
            momx_old[i][j] = momx_old[nx1-1][ny2-1];
            momy_old[i][j] = momy_old[nx1-1][ny2-1];
            ener_old[i][j] = ener_old[nx1-1][ny2-1];
        }
    }
    for (var i = nx2; i < nnx; i++) {
        for (var j = ny2; j < nny; j++) {
            dens_old[i][j] = dens_old[nx2-1][ny2-1];
            momx_old[i][j] = momx_old[nx2-1][ny2-1];
            momy_old[i][j] = momy_old[nx2-1][ny2-1];
            ener_old[i][j] = ener_old[nx2-1][ny2-1];
        }
    }*/
    
    
    for (var i = nx1-1; i < nx2+1; i++) {
        for (var j = ny1-1; j < ny2+1; j++) {
         
            varxr[0] = dens_old[i  ][j  ] - average((dens_old[i  ][j  ]-dens_old[i-1][j  ]),(dens_old[i+1][j  ]-dens_old[i  ][j  ]));
            varxr[1] = momx_old[i  ][j  ] - average((momx_old[i  ][j  ]-momx_old[i-1][j  ]),(momx_old[i+1][j  ]-momx_old[i  ][j  ]));
            varxr[2] = momy_old[i  ][j  ] - average((momy_old[i  ][j  ]-momy_old[i-1][j  ]),(momy_old[i+1][j  ]-momy_old[i  ][j  ]));
            varxr[3] = ener_old[i  ][j  ] - average((ener_old[i  ][j  ]-ener_old[i-1][j  ]),(ener_old[i+1][j  ]-ener_old[i  ][j  ]));
         
            varxl[0] = dens_old[i-1][j  ] + average((dens_old[i-1][j  ]-dens_old[i-2][j  ]),(dens_old[i  ][j  ]-dens_old[i-1][j  ]));
            varxl[1] = momx_old[i-1][j  ] + average((momx_old[i-1][j  ]-momx_old[i-2][j  ]),(momx_old[i  ][j  ]-momx_old[i-1][j  ]));
            varxl[2] = momy_old[i-1][j  ] + average((momy_old[i-1][j  ]-momy_old[i-2][j  ]),(momy_old[i  ][j  ]-momy_old[i-1][j  ]));
            varxl[3] = ener_old[i-1][j  ] + average((ener_old[i-1][j  ]-ener_old[i-2][j  ]),(ener_old[i  ][j  ]-ener_old[i-1][j  ]));
            
            varyr[0] = dens_old[i  ][j  ] - average((dens_old[i  ][j  ]-dens_old[i  ][j-1]),(dens_old[i  ][j+1]-dens_old[i  ][j  ]));
            varyr[1] = momy_old[i  ][j  ] - average((momy_old[i  ][j  ]-momy_old[i  ][j-1]),(momy_old[i  ][j+1]-momy_old[i  ][j  ]));
            varyr[2] = momx_old[i  ][j  ] - average((momx_old[i  ][j  ]-momx_old[i  ][j-1]),(momx_old[i  ][j+1]-momx_old[i  ][j  ]));
            varyr[3] = ener_old[i  ][j  ] - average((ener_old[i  ][j  ]-ener_old[i  ][j-1]),(ener_old[i  ][j+1]-ener_old[i  ][j  ]));
         
            varyl[0] = dens_old[i  ][j-1] + average((dens_old[i  ][j-1]-dens_old[i  ][j-2]),(dens_old[i  ][j  ]-dens_old[i  ][j-1]));
            varyl[1] = momy_old[i  ][j-1] + average((momy_old[i  ][j-1]-momy_old[i  ][j-2]),(momy_old[i  ][j  ]-momy_old[i  ][j-1]));
            varyl[2] = momx_old[i  ][j-1] + average((momx_old[i  ][j-1]-momx_old[i  ][j-2]),(momx_old[i  ][j  ]-momx_old[i  ][j-1]));
            varyl[3] = ener_old[i  ][j-1] + average((ener_old[i  ][j-1]-ener_old[i  ][j-2]),(ener_old[i  ][j  ]-ener_old[i  ][j-1]));
            
            riemann_solver(varxl,varxr,fluxx);
            riemann_solver(varyl,varyr,fluxy);
            
            dens_new[i  ][j] = dens_new[i  ][j] + fluxx[0]*dt;
            dens_new[i-1][j] = dens_new[i-1][j] - fluxx[0]*dt;
            momx_new[i  ][j] = momx_new[i  ][j] + fluxx[1]*dt;
            momx_new[i-1][j] = momx_new[i-1][j] - fluxx[1]*dt;
            momy_new[i  ][j] = momy_new[i  ][j] + fluxx[2]*dt;
            momy_new[i-1][j] = momy_new[i-1][j] - fluxx[2]*dt;
            ener_new[i  ][j] = ener_new[i  ][j] + fluxx[3]*dt;
            ener_new[i-1][j] = ener_new[i-1][j] - fluxx[3]*dt;
            
            dens_new[i][j  ] = dens_new[i][j  ] + fluxy[0]*dt;
            dens_new[i][j-1] = dens_new[i][j-1] - fluxy[0]*dt;
            momy_new[i][j  ] = momy_new[i][j  ] + fluxy[1]*dt;
            momy_new[i][j-1] = momy_new[i][j-1] - fluxy[1]*dt;
            momx_new[i][j  ] = momx_new[i][j  ] + fluxy[2]*dt;
            momx_new[i][j-1] = momx_new[i][j-1] - fluxy[2]*dt;
            ener_new[i][j  ] = ener_new[i][j  ] + fluxy[3]*dt;
            ener_new[i][j-1] = ener_new[i][j-1] - fluxy[3]*dt;
            
//             update_count[i][j  ] = update_count[i][j  ] + 1;
            update_count[i  ][j] = update_count[i  ][j] + 2;
            update_count[i-1][j] = update_count[i-1][j] + 1;
            update_count[i][j-1] = update_count[i][j-1] + 1;
            
            // Only need to check in i-1, not j-1 because of the order of the i,j loops
            if(update_count[i-1][j] == 4){
            
                
                dtcell = dt_cell(i-1,j);
                dtnew = Math.min(dtnew,dtcell);
                
                dens_old[i-1][j] = dens_new[i-1][j];
                momx_old[i-1][j] = momx_new[i-1][j];
                momy_old[i-1][j] = momy_new[i-1][j];
                ener_old[i-1][j] = ener_new[i-1][j];
                
                update_count[i-1][j] = 0;
                
            }
      
            
        }
    }
    
//     alert(String(dtnew)+" "+String(dt))
};
     
//###############################################################################

function riemann_solver(ucl,ucr,flux) {

    var sl,sr;
    var dl,pl,ul,vl,el;
    var dr,pr,ur,vr,er;
    var cfastl,rcl,dstarl;
    var cfastr,rcr,dstarr;
    var estarl,estarr;
    var ustar,pstar;
    var dd,uu,pp,ee;
    
    dl = ucl[0]
    ul = ucl[1] / ucl[0]
    vl = ucl[2] / ucl[0]
    el = ucl[3]
    pl = ( ucl[3] - 0.5*(ucl[1]*ucl[1]+ucl[2]*ucl[2])/ucl[0] ) * gm1
    
    dr = ucr[0]
    ur = ucr[1] / ucr[0]
    vr = ucr[2] / ucr[0]
    er = ucr[3]
    pr = ( ucr[3] - 0.5*(ucr[1]*ucr[1]+ucr[2]*ucr[2])/ucr[0] ) * gm1
    
    
    // Find the largest eigenvalues in the normal direction to the interface
    cfastl = Math.sqrt(gam*pl/dl)
    cfastr = Math.sqrt(gam*pr/dr)
    
    // Compute HLL wave speed
    sl = Math.min(ul,ur) - Math.max(cfastl,cfastr)
    sr = Math.max(ul,ur) + Math.max(cfastl,cfastr)
    
    // Compute lagrangian sound speed
    rcl = dl * (ul - sl)
    rcr = dr * (sr - ur)
    
    // Compute acoustic star state
    ustar = (rcr*ur   +rcl*ul   +  (pl-pr))/(rcr+rcl)
    pstar = (rcr*pl+rcl*pr+rcl*rcr*(ul-ur))/(rcr+rcl)
    
    // Left star region variables
    dstarl = dl*(sl-ul)/(sl-ustar)
    estarl = ((sl-ul)*el-pl*ul+pstar*ustar)/(sl-ustar)
    
    // Right star region variables
    dstarr = dr*(sr-ur)/(sr-ustar)
    estarr = ((sr-ur)*er-pr*ur+pstar*ustar)/(sr-ustar)
    
    // Sample the solution at x/t=0
    if (sl > 0.0) {
        dd = dl;
        uu = ul;
        pp = pl;
        ee = el;
    } else if (ustar > 0.0) {
        dd = dstarl;
        uu = ustar;
        pp = pstar;
        ee = estarl;
    } else if (sr > 0.0) {
        dd = dstarr;
        uu = ustar;
        pp = pstar;
        ee = estarr;
    } else {
        dd = dr;
        uu = ur;
        pp = pr;
        ee = er;
    }
    
    // Compute the Godunov flux
    flux[0] = dd*uu;
    flux[1] = dd*uu*uu + pp;
    flux[3] = (ee + pp) * uu;
    if (flux[0] > 0.0) {
        flux[2] = dd*uu*vl;
    } else {
        flux[2] = dd*uu*vr;
    }
  
};



function average(a,b) {
  
    var av = 0.0;
    
    
//     // Minmod
//     if(Math.abs(a) > Math.abs(b)) {
//         av = b;
//     } else {
//         av = a;
//     }
//     if(a*b < 0.0){
//         av = 0.0;
//     }

//      Moncen
    var dlft = 2.0*a;
    var drgt = 2.0*b;
    var dcen = (a+b)/2.0;
    var dsgn = Math.sign(dcen);
    var dlim = Math.min(Math.abs(dlft),Math.abs(drgt));
    if((dlft*drgt) <= 0.0){
        dlim = 0.0;
    }
    av = dsgn*Math.min(dlim,Math.abs(dcen));
    
// //     Falle
//      if(a*b < 0.0){
//         av = 0.0;
//      } else {
//         var r4 = Math.abs(a) + Math.abs(b);
//         if(r4 == 0.0) {
//            av = 0.0;
//         } else {
//            av = (Math.abs(a)*b + Math.abs(b)*a) / r4;
//         }
//      }
    
    
    return 0.5*av;
  
};





function update_screen() {

    var canvas = document.getElementById("myCanvas");
    var ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, canvas.width, canvas.height);
// ctx.fillStyle = "#FF0000";
// ctx.fillRect(0,0,150,75);
// </script>
    
    var icol;
  
    for (var i = nx1-1; i < nx2; i++) {
        for (var j = ny1-1; j < ny2; j++) {
            
//             cellid = "celli"+String(i)+"j"+String(j);       
            icol = Math.min(Math.max(Math.floor(((dens_new[i][j]-dmin)/(dmax-dmin))*(ncolors-1)),0),ncolors-1)
//             theCell = document.getElementById(cellid);
            theString = "rgb("+String(red[icol])+","+String(green[icol])+","+String(blue[icol])+")";
            ctx.fillStyle = theString;
            ctx.fillRect(i*celldx,j*celldy,celldx,celldy);
//             x = i*celldx;
//             y = j*celldy;
//             alert(theString);
//             theCell.setAttribute("fill",theString);
//             theCell.style.fill = "rgb("+red[icol]+","+green[icol]+","+blue[icol]")";
            
//             cellid = "celli"+String(i)+"j"+String(j);       
//             icol = Math.min(Math.max(Math.floor(((dens_new[i][j]-dmin)/(dmax-dmin))*(ncolors-1)),0),ncolors-1)
//             theCell = document.getElementById(cellid);
//             theString = "rgb("+String(red[icol])+","+String(green[icol])+","+String(blue[icol])+")";
// //             alert(theString);
//             theCell.setAttribute("fill",theString);
// //             theCell.style.fill = "rgb("+red[icol]+","+green[icol]+","+blue[icol]")";
        }
    }
    
};
