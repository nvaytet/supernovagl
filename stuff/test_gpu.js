// http://gpu.rocks/getting-started/

var gpu = new GPU();

// Create the GPU accelerated function from a kernel
// function that computes a single element in the
// 512 x 512 matrix (2D array). The kernel function
// is run in a parallel manner in the GPU resulting
// in very fast computations! (...sometimes)
var mat_mult = gpu.createKernel(function(A, B) {
    var sum = 0;
    var neil = 0;
    for (var i=0; i<512; i++) {
        sum += A[this.thread.y][i] * B[i][this.thread.x];
    }
    neil = myFunc(sum);
    return neil;
}).dimensions([512, 512]);

var n = 512;

var A = new Array(n);
for (var i = 0; i < n; i++) {
  A[i] = new Array(n);
}
var B = new Array(n);
for (var i = 0; i < n; i++) {
  B[i] = new Array(n);
}


for (var i = 0; i < n; i++) {
    for (var j = 0; j < n; j++) {
        A[i][j] = i+j;
        B[i][j] = i/j;
    }
}

// Perform matrix multiplication on 2 matrices of size 512 x 512
var C = mat_mult(A, B);


total = 0.0
for (var i = 0; i < n; i++) {
    for (var j = 0; j < n; j++) {
        total = total + C[i][j];
    }
}


 function myFunc(s){ var r = s*s; return r};

alert(String(total));