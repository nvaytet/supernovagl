// // Get window dimensions
// // var e     = document.documentElement;
// var g     = document.getElementsByTagName('body')[0];
// // var scalingx = 0.99;
// // var scalingy = 0.975;
// var scalingx = 1.0;
// var scalingy = 1.0;
// var windx0 = window.innerWidth || e.clientWidth || g.clientWidth;
// var windy0 = window.innerHeight|| e.clientHeight|| g.clientHeight;
// // alert(String(windx0)+" "+String(windy0))
// var windx = scalingx * windx0;
// var windy = scalingy * windy0 - 19;
// var ratio = windy / windx




var gpu = new GPU();









var windx = 1600;
var windy = 900;
var ratio = windy / windx

// Declare variables
var nvar = 4;
var ndim = 2;
var nx = 256;
var ny = Math.floor(nx * ratio);
var nghosts = 2;
var nnx = nx + 2*nghosts;
var nny = ny + 2*nghosts;
var nx1 = nghosts + 1;
var nx2 = nghosts + nx;
var ny1 = nghosts + 1;
var ny2 = nghosts + ny;
var time = 0.0;
var dt;
var it = 0;
var gam = 1.4;
var gm1 = gam - 1.0;
var time_limit = 10.0;
var cfl = 0.4;
var ncolors = 255;
var large_number = 1.00e+30;
var nout = 1;
var dmin       = 0.2;
var dmax       = 1.5;
var dtnew;
var dfix = 0.1;
var pfix = 1.0e-10;
var icell = 1;
var jcell = 1;

// var soundspeed;
// var wsx;
// var wsy;
// var dtx;
// var dty;
// var kinetic_energy;
// var internal_energy;


// Arrays

var dens_old = new Array(nnx);
for (var i = 0; i < nnx; i++) {
  dens_old[i] = new Array(nny);
}

var dens_new = new Array(nnx);
for (var i = 0; i < nnx; i++) {
  dens_new[i] = new Array(nny);
}

var momx_old = new Array(nnx);
for (var i = 0; i < nnx; i++) {
  momx_old[i] = new Array(nny);
}

var momx_new = new Array(nnx);
for (var i = 0; i < nnx; i++) {
  momx_new[i] = new Array(nny);
}

var momy_old = new Array(nnx);
for (var i = 0; i < nnx; i++) {
  momy_old[i] = new Array(nny);
}

var momy_new = new Array(nnx);
for (var i = 0; i < nnx; i++) {
  momy_new[i] = new Array(nny);
}

var ener_old = new Array(nnx);
for (var i = 0; i < nnx; i++) {
  ener_old[i] = new Array(nny);
}

var ener_new = new Array(nnx);
for (var i = 0; i < nnx; i++) {
  ener_new[i] = new Array(nny);
}

var update_count = new Array(nnx);
for (var i = 0; i < nnx; i++) {
  update_count[i] = new Array(nny);
}

var red   = new Array(ncolors);
var green = new Array(ncolors);
var blue  = new Array(ncolors);

// var varxl = new Array(nvar);
// var varxr = new Array(nvar);
// var varyl = new Array(nvar);
// var varyr = new Array(nvar);
// var fluxx = new Array(nvar);
// var fluxy = new Array(nvar);

// Gaussian kernel ######################################
var kw = 20;
var ngk = 2*kw + 1;
var gkernel = new Array(ngk);
for (var i = 0; i < ngk; i++) {
  gkernel[i] = new Array(ngk);
}
var x = 0.0;
var gsum = 0.0;
var x0 = (ngk+1)*0.5;
var y  = x0*x0/(16.0*Math.log(2.0));
for (var i = 0; i < ngk; i++) {
    for (var j = 0; j < ngk; j++) {
        x = ((i+1)-x0)*((i+1)-x0) + ((j+1)-x0)*((j+1)-x0);
        gkernel[i][j] = Math.exp(-x/y);
        gsum = gsum + gkernel[i][j];
    }
}
for (var i = 0; i < ngk; i++) {
    for (var j = 0; j < ngk; j++) {
       gkernel[i][j] = gkernel[i][j]/gsum;
    }
}


//###############################################################################
// Generate canvas
//###############################################################################

// Determine cell sizes in pixels
var celldx = windx / nx;
var celldy = windy / ny;
var cellid;

document.write("<canvas id=\"myCanvas\" width=\""+windx+"\" height=\""+windy+"\" style=\"border:0px;margin:0px;padding:0px;\">Your browser does not support the canvas element.</canvas>");

// document.write("<svg width=\""+windx+"\" height=\""+windy+"\">");
// for (var i = 0; i < nx; i++) {
//    for (var j = 0; j < ny; j++) {
//        x = i*celldx;
//        y = j*celldy;
//        cellid = "celli"+String(i+nghosts)+"j"+String(j+nghosts);       
//        document.write("<rect id=\""+cellid+"\" x=\""+x+"\" y=\""+y+"\" width=\""+celldx+"\" height=\""+celldy+"\" />");
//    }
// }
// document.write("</svg>");








//###############################################################################
// Generate colormap
//###############################################################################

var gradient = 0.0103;
var offset = 0.0318;
for (var i = 0; i < ncolors; i++) {
    red[i]   = Math.min(Math.max(Math.floor(ncolors*(gradient*(i-1) + offset    )),0),ncolors)
    green[i] = Math.min(Math.max(Math.floor(ncolors*(gradient*(i-1) + offset-1.0)),0),ncolors)
    blue[i]  = Math.min(Math.max(Math.floor(ncolors*(gradient*(i-1) + offset-2.0)),0),ncolors)
}


//###############################################################################
// Simulation setup
//###############################################################################
             
var d1 =  1.0;
var p1 =  1.0;
var p2 = 500.0;
  
for (var i = 0; i < nnx; i++) {
    for (var j = 0; j < nny; j++) {
        
        // Conservative variables
        dens_old[i][j] = d1;
        momx_old[i][j] = 0.0;
        momy_old[i][j] = 0.0;
        ener_old[i][j] = p1 / gm1;
     
//         dist = Math.sqrt((i-0.5*nnx)*(i-0.5*nnx)+(j-0.5*nny)*(j-0.5*nny));
//         if (dist < 0.05*nnx) {
//            ener_old[i][j] = p2 / gm1;
//            dens_old[i][j] = 2.0*d1;
//            alert(String(i)+String(j)+String(dens_old[i][j]))
//         } else {
//            ener_old[i][j] = p1 / gm1;
//         }
        
        dens_new[i][j] = dens_old[i][j];
        momx_new[i][j] = momx_old[i][j];
        momy_new[i][j] = momy_old[i][j];
        ener_new[i][j] = ener_old[i][j];
        
        update_count[i][j] = 0;
    }
}

// update_screen();



var theCanvas = document.getElementById('myCanvas');
theCanvas.addEventListener("click", getClickPosition, false);
function getClickPosition(e) {
    var rect = theCanvas.getBoundingClientRect();
//       x: evt.clientX - rect.left,
//       y: evt.clientY - rect.top
//     var xPosition = Math.round((e.clientX-rect.left)/(rect.right -rect.left)*theCanvas.width )/celldx;
//     var yPosition = Math.round((e.clientY-rect.top )/(rect.bottom-rect.top )*theCanvas.height)/celldy;
    icell = Math.round(e.clientX/celldx);
    jcell = Math.round(e.clientY/celldy);
    
//     alert(String(icell)+" "+String(jcell));
    injectEnergy();
    var dens_image = run_simulation();
    
    
    update_screen(dens_image);
//     
//     var xPosition = e.clientX - rect.left;
//     var yPosition = e.clientY - rect.top;
//     alert(String(xPosition)+" "+String(yPosition)+" "+String(theCanvas.width)+" "+String(theCanvas.height));
//     if (xPosition >= previewxmin && xPosition <= previewxmax) {
//         activate_transitions();
//         var selectImage = parseInt((xPosition-previewxmin)/previewElementSixeX);
//         posx = -imagewidth*selectImage + hidepanelwidth;
//         icap = selectImage;
//         scroll();
//     }
};


function injectEnergy() {
    // var dx = 1
    var i1 = Math.max(icell-kw,nx1);
    var i2 = Math.min(icell+kw,nx2);
    var j1 = Math.max(jcell-kw,ny1);
    var j2 = Math.min(jcell+kw,ny2);

    for (var i = i1; i < i2+1; i++) {
        for (var j = j1; j < j2+1; j++) {
            
            ener_old[i][j] = ener_old[i][j] + p2 * gkernel[i-icell+kw][j-jcell+kw] / gm1;
            ener_new[i][j] = ener_old[i][j];
        }
    }
};

// // var gpu = new GPU();
// 
// // Create the GPU accelerated function from a kernel
// // function that computes a single element in the
// // 512 x 512 matrix (2D array). The kernel function
// // is run in a parallel manner in the GPU resulting
// // in very fast computations! (...sometimes)
// var mat_mult = gpu.createKernel(function(A, B) {
//     var sum = 0;
//     var neil = 0;
//     for (var i=0; i<512; i++) {
//         sum += A[this.thread.y][i] * B[i][this.thread.x];
//     }
//     neil = myFunc(sum);
//     return neil;
// }).dimensions([512, 512]);



var run_simulation = gpu.createKernel(function(dens_new,momx_new,momy_new,ener_new,dens_old,momx_old,momy_old,ener_old) {

var time = 0.0
var it = 0;

// Declare variables
var nvar = 4;
var ndim = 2;
var nx = 256;
var ny = Math.floor(nx * ratio);
var nghosts = 2;
var nnx = nx + 2*nghosts;
var nny = ny + 2*nghosts;
var nx1 = nghosts + 1;
var nx2 = nghosts + nx;
var ny1 = nghosts + 1;
var ny2 = nghosts + ny;
// var time = 0.0;
var dt;
// var it = 0;
var gam = 1.4;
var gm1 = gam - 1.0;
var time_limit = 10.0;
var cfl = 0.4;
var ncolors = 255;
var large_number = 1.00e+30;
var nout = 1;
var dmin       = 0.2;
var dmax       = 1.5;
var dtnew;
var dfix = 0.1;
var pfix = 1.0e-10;
var icell = 1;
var jcell = 1;

var varxl = new Array(nvar);
var varxr = new Array(nvar);
var varyl = new Array(nvar);
var varyr = new Array(nvar);
var fluxx = new Array(nvar);
var fluxy = new Array(nvar);

// update_screen();

// alert("here01");

// Compute initial timestep

var soundspeed;
var wsx;
var wsy;
var dtx;
var dty;
var kinetic_energy;
var internal_energy;
dt = large_number;
for (var i = nx1-1; i < nx2; i++) {
    for (var j = ny1-1; j < ny2; j++) {
        
        
        // Compute sound speed inside cell
        kinetic_energy = 0.5 * (momx_new[i][j]*momx_new[i][j] + momy_new[i][j]*momy_new[i][j]) / dens_new[i][j];
        internal_energy = ener_new[i][j] - kinetic_energy;
        soundspeed = Math.sqrt( gam * gm1 * internal_energy / dens_new[i][j] );
        wsx = Math.abs(momx_new[i][j])/dens_new[i][j] + soundspeed;
        wsy = Math.abs(momy_new[i][j])/dens_new[i][j] + soundspeed;
        wsy = Math.abs(momy_new[i][j])/dens_new[i][j] + soundspeed;
        wsy = Math.abs(momy_new[i][j])/dens_new[i][j] + soundspeed;
        wsy = Math.abs(momy_new[i][j])/dens_new[i][j] + soundspeed;
        dtx = 1.0 / wsx;
        dty = 1.0 / wsy;
        dt = Math.min(dt,dtx,dty);
        
    }
}
dt = cfl * dt // CFL condition


var dlft = 0.0;
var drgt = 0.0;
var dcen = 0.0;
var dsgn = 0.0;
var dlim = 0.0;
var sl,sr,av;
var dl,pl,ul,vl,el;
var dr,pr,ur,vr,er;
var cfastl,rcl,dstarl;
var cfastr,rcr,dstarr;
var estarl,estarr;
var ustar,pstar;
var dd,uu,pp,ee;
// var dtcell;
// Hydro loop
while (time < time_limit) {
    
    it = it + 1;
    
//     alert("here02 "+String(time));

    
//     compute_timestep();
  
//     compute_hydro();
    
    
    
    dtnew = large_number;
//     alert("II"+String(dt_new)+" "+String(large_number))
    
    // fill ghosts cells
    for (var j = ny1-1; j < ny2; j++) {
        for (var i = 0; i < nghosts; i++) {
            dens_old[i][j] = dens_old[nx1-1][j];
            momx_old[i][j] = momx_old[nx1-1][j];
            momy_old[i][j] = momy_old[nx1-1][j];
            ener_old[i][j] = ener_old[nx1-1][j];
        }
        for (var i = nx2; i < nnx; i++) {
            dens_old[i][j] = dens_old[nx2-1][j];
            momx_old[i][j] = momx_old[nx2-1][j];
            momy_old[i][j] = momy_old[nx2-1][j];
            ener_old[i][j] = ener_old[nx2-1][j];
        }
    }
    
    for (var i = nx1-1; i < nx2; i++) {
        for (var j = 0; j < nghosts; j++) {
            dens_old[i][j] = dens_old[i][ny1-1];
            momx_old[i][j] = momx_old[i][ny1-1];
            momy_old[i][j] = momy_old[i][ny1-1];
            ener_old[i][j] = ener_old[i][ny1-1];
        }
        for (var j = ny2; j < nny; j++) {
            dens_old[i][j] = dens_old[i][ny2-1];
            momx_old[i][j] = momx_old[i][ny2-1];
            momy_old[i][j] = momy_old[i][ny2-1];
            ener_old[i][j] = ener_old[i][ny2-1];
        }
    }
    
    for (var i = nx1-1; i < nx2+1; i++) {
        for (var j = ny1-1; j < ny2+1; j++) {
            
            a01 = dens_old[i  ][j  ]-dens_old[i-1][j  ];
            b01 = dens_old[i+1][j  ]-dens_old[i  ][j  ];
            a02 = momx_old[i  ][j  ]-momx_old[i-1][j  ];
            b02 = momx_old[i+1][j  ]-momx_old[i  ][j  ];
            a03 = momy_old[i  ][j  ]-momy_old[i-1][j  ];
            b03 = momy_old[i+1][j  ]-momy_old[i  ][j  ];
            a04 = ener_old[i  ][j  ]-ener_old[i-1][j  ];
            b04 = ener_old[i+1][j  ]-ener_old[i  ][j  ];
            
            a05 = dens_old[i-1][j  ]-dens_old[i-2][j  ];
            b05 = dens_old[i  ][j  ]-dens_old[i-1][j  ];
            a06 = momx_old[i-1][j  ]-momx_old[i-2][j  ];
            b06 = momx_old[i  ][j  ]-momx_old[i-1][j  ];
            a07 = momy_old[i-1][j  ]-momy_old[i-2][j  ];
            b07 = momy_old[i  ][j  ]-momy_old[i-1][j  ];
            a08 = ener_old[i-1][j  ]-ener_old[i-2][j  ];
            b08 = ener_old[i  ][j  ]-ener_old[i-1][j  ];
            
            a09 = dens_old[i  ][j  ]-dens_old[i  ][j-1];
            b09 = dens_old[i  ][j+1]-dens_old[i  ][j  ];
            a10 = momy_old[i  ][j  ]-momy_old[i  ][j-1];
            b10 = momy_old[i  ][j+1]-momy_old[i  ][j  ];
            a11 = momx_old[i  ][j  ]-momx_old[i  ][j-1];
            b11 = momx_old[i  ][j+1]-momx_old[i  ][j  ];
            a12 = ener_old[i  ][j  ]-ener_old[i  ][j-1];
            b12 = ener_old[i  ][j+1]-ener_old[i  ][j  ];
            
            a13 = dens_old[i  ][j-1]-dens_old[i  ][j-2];
            b13 = dens_old[i  ][j  ]-dens_old[i  ][j-1];
            a14 = momy_old[i  ][j-1]-momy_old[i  ][j-2];
            b14 = momy_old[i  ][j  ]-momy_old[i  ][j-1];
            a15 = momx_old[i  ][j-1]-momx_old[i  ][j-2];
            b15 = momx_old[i  ][j  ]-momx_old[i  ][j-1];
            a16 = ener_old[i  ][j-1]-ener_old[i  ][j-2];
            b16 = ener_old[i  ][j  ]-ener_old[i  ][j-1];
            
            
            dlft = 2.0*a01;
            drgt = 2.0*b01;
            dcen = (a01+b01)/2.0;
            dsgn = Math.sign(dcen);
            dlim = Math.min(Math.abs(dlft),Math.abs(drgt));
            if((dlft*drgt) <= 0.0){
                dlim = 0.0;
            }
            av = 0.5*dsgn*Math.min(dlim,Math.abs(dcen));
            varxr[0] = dens_old[i  ][j  ] - av;
            
            dlft = 2.0*a02;
            drgt = 2.0*b02;
            dcen = (a02+b02)/2.0;
            dsgn = Math.sign(dcen);
            dlim = Math.min(Math.abs(dlft),Math.abs(drgt));
            if((dlft*drgt) <= 0.0){
                dlim = 0.0;
            }
            av = 0.5*dsgn*Math.min(dlim,Math.abs(dcen));
            varxr[1] = momx_old[i  ][j  ] - av;
            
            dlft = 2.0*a03;
            drgt = 2.0*b03;
            dcen = (a03+b03)/2.0;
            dsgn = Math.sign(dcen);
            dlim = Math.min(Math.abs(dlft),Math.abs(drgt));
            if((dlft*drgt) <= 0.0){
                dlim = 0.0;
            }
            av = 0.5*dsgn*Math.min(dlim,Math.abs(dcen));
            varxr[2] = momy_old[i  ][j  ] - av;
            
            dlft = 2.0*a04;
            drgt = 2.0*b04;
            dcen = (a04+b04)/2.0;
            dsgn = Math.sign(dcen);
            dlim = Math.min(Math.abs(dlft),Math.abs(drgt));
            if((dlft*drgt) <= 0.0){
                dlim = 0.0;
            }
            av = 0.5*dsgn*Math.min(dlim,Math.abs(dcen));
            varxr[3] = ener_old[i  ][j  ] - av;
         
            
            dlft = 2.0*a05;
            drgt = 2.0*b05;
            dcen = (a05+b05)/2.0;
            dsgn = Math.sign(dcen);
            dlim = Math.min(Math.abs(dlft),Math.abs(drgt));
            if((dlft*drgt) <= 0.0){
                dlim = 0.0;
            }
            av = 0.5*dsgn*Math.min(dlim,Math.abs(dcen));
            varxl[0] = dens_old[i-1][j  ] + av;
            
            dlft = 2.0*a06;
            drgt = 2.0*b06;
            dcen = (a06+b06)/2.0;
            dsgn = Math.sign(dcen);
            dlim = Math.min(Math.abs(dlft),Math.abs(drgt));
            if((dlft*drgt) <= 0.0){
                dlim = 0.0;
            }
            av = 0.5*dsgn*Math.min(dlim,Math.abs(dcen));
            varxl[1] = momx_old[i-1][j  ] + av;
            
            dlft = 2.0*a07;
            drgt = 2.0*b07;
            dcen = (a07+b07)/2.0;
            dsgn = Math.sign(dcen);
            dlim = Math.min(Math.abs(dlft),Math.abs(drgt));
            if((dlft*drgt) <= 0.0){
                dlim = 0.0;
            }
            av = 0.5*dsgn*Math.min(dlim,Math.abs(dcen));
            varxl[2] = momy_old[i-1][j  ] + av;
            
            dlft = 2.0*a08;
            drgt = 2.0*b08;
            dcen = (a08+b08)/2.0;
            dsgn = Math.sign(dcen);
            dlim = Math.min(Math.abs(dlft),Math.abs(drgt));
            if((dlft*drgt) <= 0.0){
                dlim = 0.0;
            }
            av = 0.5*dsgn*Math.min(dlim,Math.abs(dcen));
            varxl[3] = ener_old[i-1][j  ] + av;
            
            dlft = 2.0*a09;
            drgt = 2.0*b09;
            dcen = (a09+b09)/2.0;
            dsgn = Math.sign(dcen);
            dlim = Math.min(Math.abs(dlft),Math.abs(drgt));
            if((dlft*drgt) <= 0.0){
                dlim = 0.0;
            }
            av = 0.5*dsgn*Math.min(dlim,Math.abs(dcen));
            
            varyr[0] = dens_old[i  ][j  ] - av;
            
            dlft = 2.0*a10;
            drgt = 2.0*b10;
            dcen = (a10+b10)/2.0;
            dsgn = Math.sign(dcen);
            dlim = Math.min(Math.abs(dlft),Math.abs(drgt));
            if((dlft*drgt) <= 0.0){
                dlim = 0.0;
            }
            av = 0.5*dsgn*Math.min(dlim,Math.abs(dcen));
            varyr[1] = momy_old[i  ][j  ] - av;
            
            dlft = 2.0*a11;
            drgt = 2.0*b11;
            dcen = (a11+b11)/2.0;
            dsgn = Math.sign(dcen);
            dlim = Math.min(Math.abs(dlft),Math.abs(drgt));
            if((dlft*drgt) <= 0.0){
                dlim = 0.0;
            }
            av = 0.5*dsgn*Math.min(dlim,Math.abs(dcen));
            varyr[2] = momx_old[i  ][j  ] - av;
            
            dlft = 2.0*a12;
            drgt = 2.0*b12;
            dcen = (a12+b12)/2.0;
            dsgn = Math.sign(dcen);
            dlim = Math.min(Math.abs(dlft),Math.abs(drgt));
            if((dlft*drgt) <= 0.0){
                dlim = 0.0;
            }
            av = 0.5*dsgn*Math.min(dlim,Math.abs(dcen));
            varyr[3] = ener_old[i  ][j  ] - av;
            
            dlft = 2.0*a13;
            drgt = 2.0*b13;
            dcen = (a13+b13)/2.0;
            dsgn = Math.sign(dcen);
            dlim = Math.min(Math.abs(dlft),Math.abs(drgt));
            if((dlft*drgt) <= 0.0){
                dlim = 0.0;
            }
            av = 0.5*dsgn*Math.min(dlim,Math.abs(dcen));
         
            varyl[0] = dens_old[i  ][j-1] + av;
            
            dlft = 2.0*a14;
            drgt = 2.0*b14;
            dcen = (a14+b14)/2.0;
            dsgn = Math.sign(dcen);
            dlim = Math.min(Math.abs(dlft),Math.abs(drgt));
            if((dlft*drgt) <= 0.0){
                dlim = 0.0;
            }
            av = 0.5*dsgn*Math.min(dlim,Math.abs(dcen));
            varyl[1] = momy_old[i  ][j-1] + av;
            
            dlft = 2.0*a15;
            drgt = 2.0*b15;
            dcen = (a15+b15)/2.0;
            dsgn = Math.sign(dcen);
            dlim = Math.min(Math.abs(dlft),Math.abs(drgt));
            if((dlft*drgt) <= 0.0){
                dlim = 0.0;
            }
            av = 0.5*dsgn*Math.min(dlim,Math.abs(dcen));
            varyl[2] = momx_old[i  ][j-1] + av;
            
            dlft = 2.0*a16;
            drgt = 2.0*b16;
            dcen = (a16+b16)/2.0;
            dsgn = Math.sign(dcen);
            dlim = Math.min(Math.abs(dlft),Math.abs(drgt));
            if((dlft*drgt) <= 0.0){
                dlim = 0.0;
            }
            av = 0.5*dsgn*Math.min(dlim,Math.abs(dcen));
            varyl[3] = ener_old[i  ][j-1] + av;
            
            
            
            
            
            
            
            
            dl = varxl[0]
            ul = varxl[1] / varxl[0]
            vl = varxl[2] / varxl[0]
            el = varxl[3]
            pl = ( varxl[3] - 0.5*(varxl[1]*varxl[1]+varxl[2]*varxl[2])/varxl[0] ) * gm1
            dr = varxr[0]
            ur = varxr[1] / varxr[0]
            vr = varxr[2] / varxr[0]
            er = varxr[3]
            pr = ( varxr[3] - 0.5*(varxr[1]*varxr[1]+varxr[2]*varxr[2])/varxr[0] ) * gm1
            // Find the largest eigenvalues in the normal direction to the interface
            cfastl = Math.sqrt(gam*pl/dl)
            cfastr = Math.sqrt(gam*pr/dr)
            // Compute HLL wave speed
            sl = Math.min(ul,ur) - Math.max(cfastl,cfastr)
            sr = Math.max(ul,ur) + Math.max(cfastl,cfastr)
            // Compute lagrangian sound speed
            rcl = dl * (ul - sl)
            rcr = dr * (sr - ur)
            // Compute acoustic star state
            ustar = (rcr*ur   +rcl*ul   +  (pl-pr))/(rcr+rcl)
            pstar = (rcr*pl+rcl*pr+rcl*rcr*(ul-ur))/(rcr+rcl)
            // Left star region variables
            dstarl = dl*(sl-ul)/(sl-ustar)
            estarl = ((sl-ul)*el-pl*ul+pstar*ustar)/(sl-ustar)
            // Right star region variables
            dstarr = dr*(sr-ur)/(sr-ustar)
            estarr = ((sr-ur)*er-pr*ur+pstar*ustar)/(sr-ustar)
            // Sample the solution at x/t=0
            if (sl > 0.0) {
                dd = dl;
                uu = ul;
                pp = pl;
                ee = el;
            } else if (ustar > 0.0) {
                dd = dstarl;
                uu = ustar;
                pp = pstar;
                ee = estarl;
            } else if (sr > 0.0) {
                dd = dstarr;
                uu = ustar;
                pp = pstar;
                ee = estarr;
            } else {
                dd = dr;
                uu = ur;
                pp = pr;
                ee = er;
            }
            // Compute the Godunov flux
            fluxx[0] = dd*uu;
            fluxx[1] = dd*uu*uu + pp;
            fluxx[3] = (ee + pp) * uu;
            if (fluxx[0] > 0.0) {
                fluxx[2] = dd*uu*vl;
            } else {
                fluxx[2] = dd*uu*vr;
            }
            
            dl = varyl[0]
            ul = varyl[1] / varyl[0]
            vl = varyl[2] / varyl[0]
            el = varyl[3]
            pl = ( varyl[3] - 0.5*(varyl[1]*varyl[1]+varyl[2]*varyl[2])/varyl[0] ) * gm1
            dr = varyr[0]
            ur = varyr[1] / varyr[0]
            vr = varyr[2] / varyr[0]
            er = varyr[3]
            pr = ( varyr[3] - 0.5*(varyr[1]*varyr[1]+varyr[2]*varyr[2])/varyr[0] ) * gm1
            // Find the largest eigenvalues in the normal direction to the interface
            cfastl = Math.sqrt(gam*pl/dl)
            cfastr = Math.sqrt(gam*pr/dr)
            // Compute HLL wave speed
            sl = Math.min(ul,ur) - Math.max(cfastl,cfastr)
            sr = Math.max(ul,ur) + Math.max(cfastl,cfastr)
            // Compute lagrangian sound speed
            rcl = dl * (ul - sl)
            rcr = dr * (sr - ur)
            // Compute acoustic star state
            ustar = (rcr*ur   +rcl*ul   +  (pl-pr))/(rcr+rcl)
            pstar = (rcr*pl+rcl*pr+rcl*rcr*(ul-ur))/(rcr+rcl)
            // Left star region variables
            dstarl = dl*(sl-ul)/(sl-ustar)
            estarl = ((sl-ul)*el-pl*ul+pstar*ustar)/(sl-ustar)
            // Right star region variables
            dstarr = dr*(sr-ur)/(sr-ustar)
            estarr = ((sr-ur)*er-pr*ur+pstar*ustar)/(sr-ustar)
            // Sample the solution at x/t=0
            if (sl > 0.0) {
                dd = dl;
                uu = ul;
                pp = pl;
                ee = el;
            } else if (ustar > 0.0) {
                dd = dstarl;
                uu = ustar;
                pp = pstar;
                ee = estarl;
            } else if (sr > 0.0) {
                dd = dstarr;
                uu = ustar;
                pp = pstar;
                ee = estarr;
            } else {
                dd = dr;
                uu = ur;
                pp = pr;
                ee = er;
            }
            // Compute the Godunov flux
            fluxy[0] = dd*uu;
            fluxy[1] = dd*uu*uu + pp;
            fluxy[3] = (ee + pp) * uu;
            if (fluxy[0] > 0.0) {
                fluxy[2] = dd*uu*vl;
            } else {
                fluxy[2] = dd*uu*vr;
            }
            
            
            
            
            
            
            
            dens_new[i  ][j] = dens_new[i  ][j] + fluxx[0]*dt;
            dens_new[i-1][j] = dens_new[i-1][j] - fluxx[0]*dt;
            momx_new[i  ][j] = momx_new[i  ][j] + fluxx[1]*dt;
            momx_new[i-1][j] = momx_new[i-1][j] - fluxx[1]*dt;
            momy_new[i  ][j] = momy_new[i  ][j] + fluxx[2]*dt;
            momy_new[i-1][j] = momy_new[i-1][j] - fluxx[2]*dt;
            ener_new[i  ][j] = ener_new[i  ][j] + fluxx[3]*dt;
            ener_new[i-1][j] = ener_new[i-1][j] - fluxx[3]*dt;
            
            dens_new[i][j  ] = dens_new[i][j  ] + fluxy[0]*dt;
            dens_new[i][j-1] = dens_new[i][j-1] - fluxy[0]*dt;
            momy_new[i][j  ] = momy_new[i][j  ] + fluxy[1]*dt;
            momy_new[i][j-1] = momy_new[i][j-1] - fluxy[1]*dt;
            momx_new[i][j  ] = momx_new[i][j  ] + fluxy[2]*dt;
            momx_new[i][j-1] = momx_new[i][j-1] - fluxy[2]*dt;
            ener_new[i][j  ] = ener_new[i][j  ] + fluxy[3]*dt;
            ener_new[i][j-1] = ener_new[i][j-1] - fluxy[3]*dt;
            
//             update_count[i][j  ] = update_count[i][j  ] + 1;
            update_count[i  ][j] = update_count[i  ][j] + 2;
            update_count[i-1][j] = update_count[i-1][j] + 1;
            update_count[i][j-1] = update_count[i][j-1] + 1;
            
            // Only need to check in i-1, not j-1 because of the order of the i,j loops
            if(update_count[i-1][j] == 4){
            
                 // Pressure fix
                var density = dens_new[i-1][j]
                var kinetic_energy = 0.5 * (momx_new[i-1][j]*momx_new[i-1][j] + momy_new[i-1][j]*momy_new[i-1][j]) / dens_new[i-1][j];
                var internal_energy = ener_new[i-1][j] - kinetic_energy;
              
                if(density < dfix){
                //      ! maintain the same velocity and pressure in cell whilst resetting density
                     dens_new[i-1][j] = dfix
                     momx_new[i-1][j] = momx_new[i-1][j] / density * dens_new[i-1][j]
                     momy_new[i-1][j] = momy_new[i-1][j] / density * dens_new[i-1][j]
                     kinetic_energy = 0.5 * (momx_new[i-1][j]*momx_new[i-1][j] + momy_new[i-1][j]*momy_new[i-1][j]) / dens_new[i-1][j];
                }
            //   ! internal energy
              if(internal_energy < pfix*kinetic_energy){
                 internal_energy = pfix * kinetic_energy
              }
              
              ener_new[i-1][j] = internal_energy + kinetic_energy
              
                soundspeed = Math.sqrt( gam * gm1 * internal_energy / dens_new[i-1][j] );
             
                wsx = Math.abs(momx_new[i-1][j])/dens_new[i-1][j] + soundspeed;
                wsy = Math.abs(momy_new[i-1][j])/dens_new[i-1][j] + soundspeed;
                
                dtx = cfl / wsx;
                dty = cfl / wsy;
             
                dtnew = Math.min(dtx,dty,dtnew);
                        
         
                
                dens_old[i-1][j] = dens_new[i-1][j];
                momx_old[i-1][j] = momx_new[i-1][j];
                momy_old[i-1][j] = momy_new[i-1][j];
                ener_old[i-1][j] = ener_new[i-1][j];
                
                update_count[i-1][j] = 0;
                
            }
      
            
        }
    }
    
    
    
    
    
    
     
    time = time + dt;
    
//     update_screen();
    
//     if (it % nout == 0) {
//         setTimeout(function(){
//             update_screen();
//         },1000);
//             alert('hit me');
//     }
    
//     alert("here03 "+String(dt)+" "+String(dtnew)+" "+String(it));
    
    dt = dtnew;
     
}

// update_screen();

return dens_new;

}).dimensions([nnx, nny]);


// }; // end run simulation






function update_screen(array) {

    var canvas = document.getElementById("myCanvas");
    var ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, canvas.width, canvas.height);
// ctx.fillStyle = "#FF0000";
// ctx.fillRect(0,0,150,75);
// </script>
    
    var icol;
  
    for (var i = nx1-1; i < nx2; i++) {
        for (var j = ny1-1; j < ny2; j++) {
            
//             cellid = "celli"+String(i)+"j"+String(j);       
            icol = Math.min(Math.max(Math.floor(((array[i][j]-dmin)/(dmax-dmin))*(ncolors-1)),0),ncolors-1)
//             theCell = document.getElementById(cellid);
            theString = "rgb("+String(red[icol])+","+String(green[icol])+","+String(blue[icol])+")";
            ctx.fillStyle = theString;
            ctx.fillRect(i*celldx,j*celldy,celldx,celldy);
//             x = i*celldx;
//             y = j*celldy;
//             alert(theString);
//             theCell.setAttribute("fill",theString);
//             theCell.style.fill = "rgb("+red[icol]+","+green[icol]+","+blue[icol]")";
            
//             cellid = "celli"+String(i)+"j"+String(j);       
//             icol = Math.min(Math.max(Math.floor(((dens_new[i][j]-dmin)/(dmax-dmin))*(ncolors-1)),0),ncolors-1)
//             theCell = document.getElementById(cellid);
//             theString = "rgb("+String(red[icol])+","+String(green[icol])+","+String(blue[icol])+")";
// //             alert(theString);
//             theCell.setAttribute("fill",theString);
// //             theCell.style.fill = "rgb("+red[icol]+","+green[icol]+","+blue[icol]")";
        }
    }
    
};
