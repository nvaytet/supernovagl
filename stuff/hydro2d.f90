module variables
  implicit none
  integer, parameter :: nvar = 4
  integer, parameter :: ndim = 3
  integer, parameter :: nx = 256
  integer, parameter :: ny = 384
  integer, parameter :: nghosts = 2
  integer, parameter :: nnx = 256 + 2*nghosts
  integer, parameter :: nny = 384 + 2*nghosts
  integer, parameter :: nx1 = nghosts + 1
  integer, parameter :: nx2 = nghosts + nx
  integer, parameter :: ny1 = nghosts + 1
  integer, parameter :: ny2 = nghosts + ny
  real, dimension(nnx,nny) :: dens_old,dens_new
  real, dimension(nnx,nny) :: momx_old,momx_new
  real, dimension(nnx,nny) :: momy_old,momy_new
  real, dimension(nnx,nny) :: ener_old,ener_new
  integer, dimension(nnx,nny) :: update_count
  real :: time,dt,dtnew,gam,gm1,time_limit,cfl = 0.2
  integer, parameter :: ncolors = 255
  integer, dimension(ncolors) :: red,green,blue
  real :: dmin,dmax
  real :: large_number = 1.00e+30
  integer :: it
end module variables

!###############################################################################

program hydro2d

  use variables

  implicit none
  
  logical :: loop
  integer :: iout,nout
  
  write(*,'(a)') '########################'
  write(*,'(a)') '        HYDRO2D         '
  write(*,'(a)') '########################'
  write(*,*)
  
  ! Parameters (can be changed by the user)
  nout       = 10   ! Write output every nout timesteps
  time_limit = 10.0  ! Simulation time limit
  dmin       = 0.2  ! Minimum density in image
  dmax       = 1.5  ! Maximum density in image
  
  ! Other variables
  loop       = .true.
  iout       = 0
  it         = 0
  time       = 0.0
  
  call setup
  
  call output(iout)

  call compute_timestep
  
  do while(loop)
  
     it = it + 1
  
!      call compute_timestep
  
     call compute_hydro
     
     time = time + dt
     
     write(*,'(a,i6,a,es10.3,a,es10.3)') 'it = ',it,'  time = ',time,'  dt = ',dt
     
     if(mod(it,nout) == 0)then
        iout = iout + 1
        call output(iout)
     endif
     
     if(time >= time_limit) loop = .false.
     
     dt = dtnew
  
  enddo

  stop

end program hydro2d

!###############################################################################

subroutine setup

  use variables

  implicit none
  
  real    :: dist,d1,d2,p1,p2,gradient,offset
  integer :: i,n,k,j

  ! Constants
  gam = 1.4
  gm1 = gam - 1.0
             
  ! Define conservative variables inside cells
  d1 =  1.0
  d2 =  2.0
  p1 =  1.0
  p2 = 10.0
  
  do j = 1,nny
     do i = 1,nnx
     
        ! Conservative variables
        dens_old(i,j) = d1
        momx_old(i,j) = 0.0
        momy_old(i,j) = 0.0
     
        dist = sqrt((real(i)-0.5*real(nnx))**2+(real(j)-0.5*real(nny))**2)
        if(dist .le. 0.1*real(nnx))then
           ener_old(i,j) = p2 / gm1
        else
           ener_old(i,j) = p1 / gm1
        endif
        
        dens_new(i,j) = dens_old(i,j)
        momx_new(i,j) = momx_old(i,j)
        momy_new(i,j) = momy_old(i,j)
        ener_new(i,j) = ener_old(i,j)
        
        update_count(i,j) = 0
        
     enddo     
  enddo
  
  ! Generate_colormap
  gradient = 0.0103 ; offset = 0.0318
  do i = 1,ncolors
     red  (i) = min(max(nint(ncolors*(gradient*real(i-1) + offset    )),0),ncolors)
     green(i) = min(max(nint(ncolors*(gradient*real(i-1) + offset-1.0)),0),ncolors)
     blue (i) = min(max(nint(ncolors*(gradient*real(i-1) + offset-2.0)),0),ncolors)
  enddo
     
  return

end subroutine setup

!###############################################################################

subroutine compute_timestep

  use variables

  implicit none

  integer :: i,j,k
  real    :: soundspeed,wsx,wsy,dtx,dty,kinetic_energy,internal_energy
  
  dt = large_number
  
  dens_old = dens_new
  momx_old = momx_new
  momy_old = momy_new
  ener_old = ener_new
  
  do j = ny1,ny2
     do i = nx1,nx2
  
!   write(*,*) i,j
        ! Compute sound speed inside cell
        kinetic_energy = 0.5 * (momx_new(i,j)**2 + momy_new(i,j)**2) / dens_new(i,j)
        
        internal_energy = ener_new(i,j) - kinetic_energy
        
     
        soundspeed = sqrt( gam * gm1 * internal_energy / dens_new(i,j) )
     
        wsx = abs(momx_new(i,j))/dens_new(i,j) + soundspeed
        wsy = abs(momy_new(i,j))/dens_new(i,j) + soundspeed
        
        dtx = 1.0 / wsx
        dty = 1.0 / wsy
     
        dt = min(dt,dtx,dty)
        
     enddo
  enddo
  
  ! CFL condition
  dt = cfl * dt

  return

end subroutine compute_timestep

!###############################################################################

function dt_cell(i,j)

  use variables

  implicit none

  integer :: i,j
  real    :: soundspeed,wsx,wsy,dtx,dty,kinetic_energy,internal_energy,dt_cell
  
!   dt = large_number
!   
!   dens_old = dens_new
!   momx_old = momx_new
!   momy_old = momy_new
!   ener_old = ener_new
  
!   do j = ny1,ny2
!      do i = nx1,nx2
  
!     if(it==9) write(*,*) i,j,momx_new(i,j)
  
        ! Compute sound speed inside cell
        kinetic_energy = 0.5 * (momx_new(i,j)**2 + momy_new(i,j)**2) / dens_new(i,j)
!     if(it==9)     write(*,*) 'ke',kinetic_energy
        internal_energy = ener_new(i,j) - kinetic_energy
     
!   if(it==9)  write(*,*) 'ie',internal_energy
        if(internal_energy < 0.0)then
        write(*,*) internal_energy

         read(*,*)
        endif
     
        soundspeed = sqrt( gam * gm1 * internal_energy / dens_new(i,j) )
     
        wsx = abs(momx_new(i,j))/dens_new(i,j) + soundspeed
        wsy = abs(momy_new(i,j))/dens_new(i,j) + soundspeed
        
        dtx = 1.0 / wsx
        dty = 1.0 / wsy
     
        dt_cell = min(dtx,dty)
        
!      enddo
!   enddo
  
  ! CFL condition
  dt_cell = cfl * dt_cell

  return

end function dt_cell

!###############################################################################

subroutine compute_hydro

  use variables
  
  implicit none
  
  integer               :: i,j
  real, dimension(nvar) :: varxl,varyl,varxc,varyc,flux_x,flux_y
  real :: dtcell,dt_cell
  
  dtnew = large_number
  
  ! fill ghosts cells
  do j = ny1,ny2
     dens_old(1:nghosts,j) = dens_old(nx1,j)
     momx_old(1:nghosts,j) = momx_old(nx1,j)
     momy_old(1:nghosts,j) = momy_old(nx1,j)
     ener_old(1:nghosts,j) = ener_old(nx1,j)
     dens_old(nx2+1:nnx,j) = dens_old(nx2,j)
     momx_old(nx2+1:nnx,j) = momx_old(nx2,j)
     momy_old(nx2+1:nnx,j) = momy_old(nx2,j)
     ener_old(nx2+1:nnx,j) = ener_old(nx2,j)
  enddo
  do i = nx1,nx2
     dens_old(i,1:nghosts) = dens_old(i,ny1)
     momx_old(i,1:nghosts) = momx_old(i,ny1)
     momy_old(i,1:nghosts) = momy_old(i,ny1)
     ener_old(i,1:nghosts) = ener_old(i,ny1)
     dens_old(i,ny2+1:nny) = dens_old(i,ny2)
     momx_old(i,ny2+1:nny) = momx_old(i,ny2)
     momy_old(i,ny2+1:nny) = momy_old(i,ny2)
     ener_old(i,ny2+1:nny) = ener_old(i,ny2)
  enddo
  do j = 1,nghosts
     do i = 1,nghosts
        dens_old(i,j) = dens_old(nx1,ny1)
        momx_old(i,j) = momx_old(nx1,ny1)
        momy_old(i,j) = momy_old(nx1,ny1)
        ener_old(i,j) = ener_old(nx1,ny1)
     enddo
  enddo
  do j = 1,nghosts
     do i = nx2+1,nnx
        dens_old(i,j) = dens_old(nx2,ny1)
        momx_old(i,j) = momx_old(nx2,ny1)
        momy_old(i,j) = momy_old(nx2,ny1)
        ener_old(i,j) = ener_old(nx2,ny1)
     enddo
  enddo
  do j = ny2+1,nny
     do i = 1,nghosts
        dens_old(i,j) = dens_old(nx1,ny2)
        momx_old(i,j) = momx_old(nx1,ny2)
        momy_old(i,j) = momy_old(nx1,ny2)
        ener_old(i,j) = ener_old(nx1,ny2)
     enddo
  enddo
  do j = ny2+1,nny
     do i = nx2+1,nnx
        dens_old(i,j) = dens_old(nx2,ny2)
        momx_old(i,j) = momx_old(nx2,ny2)
        momy_old(i,j) = momy_old(nx2,ny2)
        ener_old(i,j) = ener_old(nx2,ny2)
     enddo
  enddo
  
  do j = ny1,ny2+1
     do i = nx1,nx2+1
     
        varxc(1) = dens_old(i  ,j)
        varxc(2) = momx_old(i  ,j)
        varxc(3) = momy_old(i  ,j)
        varxc(4) = ener_old(i  ,j)
     
        varxl(1) = dens_old(i-1,j)
        varxl(2) = momx_old(i-1,j)
        varxl(3) = momy_old(i-1,j)
        varxl(4) = ener_old(i-1,j)
        
        varyc(1) = dens_old(i  ,j)
        varyc(2) = momy_old(i  ,j)
        varyc(3) = momx_old(i  ,j)
        varyc(4) = ener_old(i  ,j)
     
        varyl(1) = dens_old(i,j-1)
        varyl(2) = momy_old(i,j-1)
        varyl(3) = momx_old(i,j-1)
        varyl(4) = ener_old(i,j-1)
        
        call riemann_solver(varxl,varxc,flux_x)
        call riemann_solver(varyl,varyc,flux_y)
        
        dens_new(i  ,j) = dens_new(i  ,j) + flux_x(1)*dt
        dens_new(i-1,j) = dens_new(i-1,j) - flux_x(1)*dt
        momx_new(i  ,j) = momx_new(i  ,j) + flux_x(2)*dt
        momx_new(i-1,j) = momx_new(i-1,j) - flux_x(2)*dt
        momy_new(i  ,j) = momy_new(i  ,j) + flux_x(3)*dt
        momy_new(i-1,j) = momy_new(i-1,j) - flux_x(3)*dt
        ener_new(i  ,j) = ener_new(i  ,j) + flux_x(4)*dt
        ener_new(i-1,j) = ener_new(i-1,j) - flux_x(4)*dt
        
        dens_new(i,j  ) = dens_new(i,j  ) + flux_y(1)*dt
        dens_new(i,j-1) = dens_new(i,j-1) - flux_y(1)*dt
        momy_new(i,j  ) = momy_new(i,j  ) + flux_y(2)*dt
        momy_new(i,j-1) = momy_new(i,j-1) - flux_y(2)*dt
        momx_new(i,j  ) = momx_new(i,j  ) + flux_y(3)*dt
        momx_new(i,j-1) = momx_new(i,j-1) - flux_y(3)*dt
        ener_new(i,j  ) = ener_new(i,j  ) + flux_y(4)*dt
        ener_new(i,j-1) = ener_new(i,j-1) - flux_y(4)*dt
        
        update_count(i,j) = update_count(i,j) + 2;
        update_count(i-1,j) = update_count(i-1,j) + 1;
        update_count(i,j-1) = update_count(i,j-1) + 1;
        
!         if(update_count(i-1,j) == 4)then
!         
! !             alert("AAAAAAAAA"+String(i-1)+String(j))
! !             write(*,*) 'AAAAAAAA',i-1,j
!             dtcell = dt_cell(i-1,j);
!             dtnew = min(dtnew,dtcell);
!             
!             dens_old(i-1,j) = dens_new(i-1,j);
!             momx_old(i-1,j) = momx_new(i-1,j);
!             momy_old(i-1,j) = momy_new(i-1,j);
!             ener_old(i-1,j) = ener_new(i-1,j);
!             
!             update_count(i-1,j) = 0;
!             
! !             write(*,*) 'AAAAAAAA',i-1,j,dtcell,dtnew
! !             read(*,*)
!             
!         endif
        
        if(update_count(i,j-1) == 4)then
        
!             alert("BBBBBBB"+String(i)+String(j-1))
            
            
            dtcell = dt_cell(i,j-1);
            dtnew = min(dtnew,dtcell);
            
            dens_old(i,j-1) = dens_new(i,j-1);
            momx_old(i,j-1) = momx_new(i,j-1);
            momy_old(i,j-1) = momy_new(i,j-1);
            ener_old(i,j-1) = ener_new(i,j-1);
            
            update_count(i,j-1) = 0;
            
!             write(*,*) 'BBBBBB',i,j-1,dtcell,dtnew
            
        endif
        
     enddo
  enddo
  
  return

end subroutine compute_hydro

!###############################################################################

subroutine riemann_solver(ucl,ucr,flux)

  use variables

  implicit none

  real, dimension(nvar) :: ucl,ucr,flux

  real :: sl,sr
  real :: dl,pl,ul,vl,el
  real :: dr,pr,ur,vr,er
  real :: cfastl,rcl,dstarl
  real :: cfastr,rcr,dstarr
  real :: estarl,estarr
  real :: ustar,pstar
  real :: dd,uu,pp,ee

  dl = ucl(1)
  ul = ucl(2) / ucl(1)
  vl = ucl(3) / ucl(1)
  el = ucl(4)
  pl = ( ucl(4) - 0.5*(ucl(2)*ucl(2)+ucl(3)*ucl(3))/ucl(1) ) * gm1
  
  dr = ucr(1)
  ur = ucr(2) / ucr(1)
  vr = ucr(3) / ucr(1)
  er = ucr(4)
  pr = ( ucr(4) - 0.5*(ucr(2)*ucr(2)+ucr(3)*ucr(3))/ucr(1) ) * gm1
  

  ! Find the largest eigenvalues in the normal direction to the interface
  cfastl = sqrt(gam*pl/dl)
  cfastr = sqrt(gam*pr/dr)

  ! Compute HLL wave speed
  sl = min(ul,ur) - max(cfastl,cfastr)
  sr = max(ul,ur) + max(cfastl,cfastr)

  ! Compute lagrangian sound speed
  rcl = dl * (ul - sl)
  rcr = dr * (sr - ur)

  ! Compute acoustic star state
  ustar = (rcr*ur   +rcl*ul   +  (pl-pr))/(rcr+rcl)
  pstar = (rcr*pl+rcl*pr+rcl*rcr*(ul-ur))/(rcr+rcl)

  ! Left star region variables
  dstarl = dl*(sl-ul)/(sl-ustar)
  estarl = ((sl-ul)*el-pl*ul+pstar*ustar)/(sl-ustar)

  ! Right star region variables
  dstarr = dr*(sr-ur)/(sr-ustar)
  estarr = ((sr-ur)*er-pr*ur+pstar*ustar)/(sr-ustar)

  ! Sample the solution at x/t=0
  if(sl>0.0)then
     dd = dl
     uu = ul
     pp = pl
     ee = el
  elseif(ustar > 0.0)then
     dd = dstarl
     uu = ustar
     pp = pstar
     ee = estarl
  elseif(sr > 0.0)then
     dd = dstarr
     uu = ustar
     pp = pstar
     ee = estarr
  else
     dd = dr
     uu = ur
     pp = pr
     ee = er
  end if

  ! Compute the Godunov flux
  flux(1) = dd*uu
  flux(2) = dd*uu*uu + pp
  flux(4) = (ee + pp) * uu
  if(flux(1) > 0.0)then
     flux(3) = dd*uu*vl
  else
     flux(3) = dd*uu*vr
  endif
  
  return

end subroutine riemann_solver

!###############################################################################

subroutine output(iout)

  use variables

  implicit none
  
  integer, intent(in) :: iout
  character (len=15)  :: fname
  integer             :: i,j,icol
  
  if(iout < 10)then
     write(fname,'(a,i1,a)') 'output-000',iout,'.ppm'
  elseif(iout < 100)then
     write(fname,'(a,i2,a)') 'output-00',iout,'.ppm'
  elseif(iout < 1000)then
     write(fname,'(a,i3,a)') 'output-0',iout,'.ppm'
  else
     write(fname,'(a,i4,a)') 'output-',iout,'.ppm'
  endif
  
  open (21,file=fname,form='formatted')
  write(21,'(a)') 'P3'
  write(21,*) nx,ny
  write(21,*) ncolors
  do j = ny2,ny1,-1
     do i = nx1,nx2
        icol = min(max(int(((dens_new(i,j)-dmin)/(dmax-dmin))*(ncolors-1)) + 1,1),ncolors)
        write(21,*) red(icol),green(icol),blue(icol)
     enddo
  enddo
  close(21)
  
  return

end subroutine output
