!###############################################################################
! MINYDRO: Minimal Cell-Based Hydrodynamics
! Written by Neil Vaytet (ENS Lyon)
! Version 1.0 - July 2013
! Compile with: gfortran -o minydro minydro.f90 -ffree-line-length-none
! Add the -fopenmp flag if you wish to use OpenMP parallelisation.
!###############################################################################

!###############################################################################

module parameters
  implicit none
  integer, parameter :: dp   = 8
  integer, parameter :: nvar = 5
end module parameters

!###############################################################################

module cell_structure
  use parameters
  implicit none
  type cell
     integer , dimension(6) :: neighbour
     real(dp)               :: density
     real(dp), dimension(3) :: momentum
     real(dp), dimension(3) :: x_centre
     real(dp), dimension(3) :: x_left
     real(dp), dimension(3) :: x_right
     real(dp), dimension(5) :: update
     real(dp), dimension(6) :: area
     real(dp)               :: energy
     real(dp)               :: volume
     logical , dimension(6) :: box_edge
  end type cell
end module cell_structure

!###############################################################################

module grid_structure
  use cell_structure
  implicit none
  integer                                 :: ndim
  integer                                 :: ncells
  integer   , dimension(3  )              :: nx
  integer   , dimension(3  )              :: iodd
  integer   , dimension(3  )              :: ieven
  integer   , dimension(5,3)              :: ivar
  integer   , dimension(5  )              :: icycle
  integer   , dimension(6  )              :: boundaries
  real(dp)  , dimension(3  )              :: lbox
  type(cell), dimension(:  ), allocatable :: grid_cells
end module grid_structure

!###############################################################################

module variables
  use parameters
  implicit none
  real(dp) :: gam
  real(dp) :: gm1
  real(dp) :: dt
  real(dp) :: time
  real(dp) :: time_limit
  real(dp) :: cfl = 0.2_dp
  logical  :: use_slopes
  integer  :: ncolors
  integer, dimension(:), allocatable :: red,green,blue
  real(dp) :: dmin,dmax
end module variables

!###############################################################################

module constants
  use parameters
  implicit none
  real(dp), parameter :: zero         = 0.00e+00_dp  !<  0
  real(dp), parameter :: one          = 1.00e+00_dp  !<  1
  real(dp), parameter :: two          = 2.00e+00_dp  !<  2
  real(dp), parameter :: half         = 0.50e+00_dp  !< 1/2
  real(dp), parameter :: large_number = 1.00e+30_dp  !< A very large number
end module constants

!###############################################################################

program minydro

  use variables
  use constants

  implicit none
  
  logical :: loop
  integer :: it,iout,nout
  
  write(*,'(a)') '########################'
  write(*,'(a)') '        MINYDRO         '
  write(*,'(a)') ' Written by Neil Vaytet '
  write(*,'(a)') '     (version 1.0)      '
  write(*,'(a)') '########################'
  write(*,*)
  
  ! Parameters (can be changed by the user)
  nout       = 100      ! Write output every nout timesteps
  time_limit = 5.0_dp  ! Simulation time limit
  use_slopes = .true.  ! Use second order reconstruction?
  dmin       = 0.2_dp  ! Minimum density in image
  dmax       = 1.5_dp  ! Maximum density in image
  
  ! Other variables
  loop       = .true.
  iout       = 0
  it         = 0
  time       = zero
  
  call setup
  
  call output(iout)

  do while(loop)
  
     it = it + 1
  
     call compute_timestep
  
     call compute_hydro
     
     time = time + dt
     
     write(*,'(a,i6,a,es10.3,a,es10.3)') 'it = ',it,'  time = ',time,'  dt = ',dt
     
     if(mod(it,nout) == 0)then
        iout = iout + 1
        call output(iout)
     endif
     
     if(time >= time_limit) loop = .false.
  
  enddo

  stop

end program minydro

!###############################################################################

subroutine setup

  use cell_structure
  use grid_structure
  use variables
  use constants

  implicit none
  
  real(dp), dimension(3) :: dx
  real(dp)               :: dist,rho1,p1,p2,kinetic_energy
  integer                :: i,n,k,j

  ! Constants
  gam = 1.4_dp
  gm1 = gam - one
  
  ! Grid dimensions
  ndim  = 2
  nx(1) = 1024
  nx(2) = 1536
  nx(3) = 1
  
  ! Simulation box size
  lbox(1) = 1.0_dp
  lbox(2) = 1.5_dp
  lbox(3) = 1.0_dp
  
  ! Boundary conditions: 0 = free-flow, 1 = periodic
  boundaries = 1
  
  ! Generate cell mesh
  call generate_cells

  ! Update cell coordinates
  do k = 1,ndim
     dx(k) = lbox(k) / real(nx(k),dp)
  enddo
  
  do n = 1,ncells
     do k = 1,ndim
  
        if(grid_cells(n)%box_edge(iodd(k)))then
           grid_cells(n)%x_left  (k) = zero - half * lbox(k) ! defines the cell coordinates
           grid_cells(n)%x_right (k) = grid_cells(n)%x_left(k) + dx(k)
           grid_cells(n)%x_centre(k) = half * (grid_cells(n)%x_left(k) + grid_cells(n)%x_right(k))
        endif
        
     enddo
  enddo
  
  do n = 1,ncells
     do k = 1,ndim
     
        if(.not. grid_cells(n)%box_edge(iodd(k)))then
           i = grid_cells(n)%neighbour(iodd(k))
           grid_cells(n)%x_left  (k) = grid_cells(i)%x_right(k)
           grid_cells(n)%x_right (k) = grid_cells(n)%x_left(k) + dx(k)
           grid_cells(n)%x_centre(k) = half * (grid_cells(n)%x_left(k) + grid_cells(n)%x_right(k))
        endif
        
     enddo
  enddo
  
  ! Compute cell face areas and volume
  do n = 1,ncells
     grid_cells(n)%area   = one
     grid_cells(n)%volume = one
     do k = 1,ndim
        do j = 1,ndim-1
           grid_cells(n)%area(iodd (k)) = grid_cells(n)%area(iodd (k)) * (grid_cells(n)%x_right(icycle(k+j)) - grid_cells(n)%x_left(icycle(k+j)))
           grid_cells(n)%area(ieven(k)) = grid_cells(n)%area(ieven(k)) * (grid_cells(n)%x_right(icycle(k+j)) - grid_cells(n)%x_left(icycle(k+j)))
        enddo
        grid_cells(n)%volume = grid_cells(n)%volume * (grid_cells(n)%x_right(k) - grid_cells(n)%x_left(k))
     enddo
  enddo
           
  ! Define conservative variables inside cells
  rho1 =  1.0_dp
  p1   =  1.0_dp
  p2   = 10.0_dp
  
  do n = 1,ncells
  
     ! Conservative variables
     grid_cells(n)%density  = rho1
     grid_cells(n)%momentum = zero
     
     dist = zero
     do k = 1,ndim
        dist = dist + grid_cells(n)%x_centre(k)**2
     enddo
     dist = sqrt(dist)
     if(dist .le. 0.1_dp)then
        grid_cells(n)%energy = p2 / gm1
     else
        grid_cells(n)%energy = p1 / gm1
     endif
     
     ! Update total energy
     kinetic_energy = zero
     do k = 1,ndim
        kinetic_energy = kinetic_energy + grid_cells(n)%momentum(k)*grid_cells(n)%momentum(k)
     enddo
     kinetic_energy = half * kinetic_energy / grid_cells(n)%density
     
     grid_cells(n)%energy = grid_cells(n)%energy + kinetic_energy
     
     ! Make sure update is zero for first timestep
     grid_cells(n)%update = zero
     
  enddo
  
  call generate_colormap
     
  return

end subroutine setup

!###############################################################################

subroutine generate_cells

  use cell_structure
  use grid_structure

  implicit none
  
  integer :: n,k,nmod
  
  ! Count the number of cells
  ncells = 1
  do n = 1,ndim
     ncells = ncells * nx(n)
  enddo
  
  ! Allocate memory space
  allocate(grid_cells(ncells))
  
  ! Define cell ids
  do n = 1,ncells
     grid_cells(n)%box_edge = .false.
  enddo
  
  ! Find cell neighbours
  do n = 1,ncells
  
     grid_cells(n)%neighbour(1) = n - 1
     grid_cells(n)%neighbour(2) = n + 1     
     grid_cells(n)%neighbour(3) = n - nx(1)
     grid_cells(n)%neighbour(4) = n + nx(1)
     grid_cells(n)%neighbour(5) = n - nx(1)*nx(2)
     grid_cells(n)%neighbour(6) = n + nx(1)*nx(2)     
     
     if(nmod(n,nx(1)            ) .eq. 1                    )then
        grid_cells(n)%neighbour(1) = n -             (1-nx(1))*boundaries(1)
        grid_cells(n)%box_edge(1) = .true.
     endif
     if(nmod(n,nx(1)            ) .eq. nx(1)                )then
        grid_cells(n)%neighbour(2) = n +             (1-nx(1))*boundaries(2)
        grid_cells(n)%box_edge(2) = .true.
     endif
     if(nmod(n,nx(1)*nx(2)      ) .le. nx(1)                )then
        grid_cells(n)%neighbour(3) = n - nx(1)*      (1-nx(2))*boundaries(3)
        grid_cells(n)%box_edge(3) = .true.
     endif
     if(nmod(n,nx(1)*nx(2)      ) .gt. nx(1)      *(nx(2)-1))then
        grid_cells(n)%neighbour(4) = n + nx(1)*      (1-nx(2))*boundaries(4)
        grid_cells(n)%box_edge(4) = .true.
     endif
     if(nmod(n,nx(1)*nx(2)*nx(3)) .le. nx(1)*nx(2)          )then
        grid_cells(n)%neighbour(5) = n - nx(1)*nx(2)*(1-nx(3))*boundaries(5)
        grid_cells(n)%box_edge(5) = .true.
     endif
     if(nmod(n,nx(1)*nx(2)*nx(3)) .gt. nx(1)*nx(2)*(nx(3)-1))then
        grid_cells(n)%neighbour(6) = n + nx(1)*nx(2)*(1-nx(3))*boundaries(6)
        grid_cells(n)%box_edge(6) = .true.
     endif
     
  enddo
  
  ! Initialise cyclic arrays
  do k = 1,ndim
     iodd (k) = 1+(k-1)*2
     ieven(k) = k*2
  enddo
  
  do k = 1,2*ndim-1
     icycle(k) = nmod(k,ndim)
  enddo
  
  ! Ivar matrix for flux indices
  ivar(1,1) = 1 ; ivar(1,2) = 1 ; ivar(1,3) = 1
  ivar(2,1) = 2 ; ivar(2,2) = 3 ; ivar(2,3) = 4
  ivar(3,1) = 3 ; ivar(3,2) = 4 ; ivar(3,3) = 2
  ivar(4,1) = 4 ; ivar(4,2) = 2 ; ivar(4,3) = 3
  ivar(5,1) = 5 ; ivar(5,2) = 5 ; ivar(5,3) = 5
  
  return
  
end subroutine generate_cells

!###############################################################################

subroutine output(iout)

  use grid_structure
  use variables

  implicit none
  
  integer, intent(in) :: iout
  character (len=15)  :: fname
  integer             :: n,icol
  real(dp) :: pmin,pmax
  
  if(iout < 10)then
     write(fname,'(a,i1,a)') 'output-000',iout,'.ppm'
  elseif(iout < 100)then
     write(fname,'(a,i2,a)') 'output-00',iout,'.ppm'
  elseif(iout < 1000)then
     write(fname,'(a,i3,a)') 'output-0',iout,'.ppm'
  else
     write(fname,'(a,i4,a)') 'output-',iout,'.ppm'
  endif
  
  open (21,file=fname,form='formatted')
  write(21,'(a)') 'P3'
  write(21,*) nx(1),nx(2)
  write(21,*) ncolors
  do n = 1,ncells
     icol = min(max(int(((grid_cells(n)%density-dmin)/(dmax-dmin))*(ncolors-1)) + 1,1),ncolors)
     write(21,*) red(icol),green(icol),blue(icol)
  enddo
  close(21)
  
  return

end subroutine output

!###############################################################################

subroutine compute_timestep

  use grid_structure
  use constants
  use variables

  implicit none

  integer  :: n,k
  real(dp) :: soundspeed,wavespeed,dt_cell,kinetic_energy,internal_energy
  
  dt = large_number
  
  !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(n,k,soundspeed,wavespeed,dt_cell,kinetic_energy,&
  !$OMP internal_energy) REDUCTION(MIN:dt)
  !$OMP DO
  do n = 1,ncells
  
     ! Update new state with conservative update
     grid_cells(n)%density = grid_cells(n)%density + grid_cells(n)%update(1)
     do k = 1,ndim
        grid_cells(n)%momentum(k) = grid_cells(n)%momentum(k) + grid_cells(n)%update(k+1)
     enddo
     grid_cells(n)%energy = grid_cells(n)%energy + grid_cells(n)%update(5)

     ! Compute sound speed inside cell
     kinetic_energy = zero
     do k = 1,ndim
        kinetic_energy = kinetic_energy + grid_cells(n)%momentum(k)*grid_cells(n)%momentum(k)
     enddo
     kinetic_energy = half * kinetic_energy / grid_cells(n)%density
     
     internal_energy = grid_cells(n)%energy - kinetic_energy
  
     soundspeed = sqrt( gam * gm1 * internal_energy / grid_cells(n)%density )
  
     do k = 1,ndim
        wavespeed = abs(grid_cells(n)%momentum(k))/grid_cells(n)%density + soundspeed
        dt_cell = (grid_cells(n)%x_right(k) - grid_cells(n)%x_left(k)) / wavespeed
        dt = min(dt,dt_cell)
     enddo

  enddo
  !$OMP END DO
  !$OMP END PARALLEL
  
  ! CFL condition
  dt = cfl * dt

  return

end subroutine compute_timestep

!###############################################################################

subroutine compute_hydro

  use grid_structure
  use constants
  use variables
  
  implicit none
  
  integer                   :: n,k,h
  real(dp), dimension(nvar) :: var_left,var_centre,var_right,flux_left,flux_right
  real(dp), dimension(nvar) :: var_left_left,var_right_right
  real(dp), dimension(nvar) :: slope_left,slope_centre,slope_right
  real(dp), dimension(nvar) :: state1,state2,state3,state4
  real(dp)                  :: dx1,dx2,slope1,slope2,minmod
  
  !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(n,k,h,var_left_left,var_left,var_centre,var_right,&
  !$OMP var_right_right,slope_left,slope_centre,slope_right,dx1,dx2,slope1,slope2,state1,  &
  !$OMP state2,state3,state4,flux_left,flux_right)
  !$OMP DO
  do n = 1,ncells

     grid_cells(n)%update = zero
  
     do k = 1,ndim
     
        ! Left cell
        var_left(1) = grid_cells(grid_cells(n)%neighbour(iodd(k)))%density
        var_left(2) = grid_cells(grid_cells(n)%neighbour(iodd(k)))%momentum(ivar(2,k)-1)
        var_left(3) = grid_cells(grid_cells(n)%neighbour(iodd(k)))%momentum(ivar(3,k)-1)
        var_left(4) = grid_cells(grid_cells(n)%neighbour(iodd(k)))%momentum(ivar(4,k)-1)
        var_left(5) = grid_cells(grid_cells(n)%neighbour(iodd(k)))%energy
        
        ! Centre cell
        var_centre(1) = grid_cells(n)%density
        var_centre(2) = grid_cells(n)%momentum(ivar(2,k)-1)
        var_centre(3) = grid_cells(n)%momentum(ivar(3,k)-1)
        var_centre(4) = grid_cells(n)%momentum(ivar(4,k)-1)
        var_centre(5) = grid_cells(n)%energy
        
        ! Right cell
        var_right(1) = grid_cells(grid_cells(n)%neighbour(ieven(k)))%density
        var_right(2) = grid_cells(grid_cells(n)%neighbour(ieven(k)))%momentum(ivar(2,k)-1)
        var_right(3) = grid_cells(grid_cells(n)%neighbour(ieven(k)))%momentum(ivar(3,k)-1)
        var_right(4) = grid_cells(grid_cells(n)%neighbour(ieven(k)))%momentum(ivar(4,k)-1)
        var_right(5) = grid_cells(grid_cells(n)%neighbour(ieven(k)))%energy
        
        ! Slopes
        slope_left   = zero
        slope_centre = zero
        slope_right  = zero
        
        if(use_slopes)then
        
           ! Left cell
           var_left_left(1) = grid_cells(grid_cells(grid_cells(n)%neighbour(iodd(k)))%neighbour(iodd(k)))%density
           var_left_left(2) = grid_cells(grid_cells(grid_cells(n)%neighbour(iodd(k)))%neighbour(iodd(k)))%momentum(ivar(2,k)-1)
           var_left_left(3) = grid_cells(grid_cells(grid_cells(n)%neighbour(iodd(k)))%neighbour(iodd(k)))%momentum(ivar(3,k)-1)
           var_left_left(4) = grid_cells(grid_cells(grid_cells(n)%neighbour(iodd(k)))%neighbour(iodd(k)))%momentum(ivar(4,k)-1)
           var_left_left(5) = grid_cells(grid_cells(grid_cells(n)%neighbour(iodd(k)))%neighbour(iodd(k)))%energy
           
           dx1 = grid_cells(grid_cells(n)%neighbour(iodd(k)))%x_centre(k) - grid_cells(grid_cells(grid_cells(n)%neighbour(iodd(k)))%neighbour(iodd(k)))%x_centre(k)
           dx2 = grid_cells(n)%x_centre(k)                                - grid_cells(grid_cells(n)%neighbour(iodd(k)))%x_centre(k)
           if(dx1*dx2 .ne. zero)then
              do h = 1,nvar
                 slope1 = ( var_left  (h) - var_left_left(h) ) / dx1
                 slope2 = ( var_centre(h) - var_left     (h) ) / dx2
                 slope_left(h) = minmod(slope1,slope2)
              enddo
           endif
        
           ! Centre cell
           dx1 = grid_cells(n)%x_centre(k)                                 - grid_cells(grid_cells(n)%neighbour(iodd(k)))%x_centre(k)
           dx2 = grid_cells(grid_cells(n)%neighbour(ieven(k)))%x_centre(k) - grid_cells(n)%x_centre(k)
           if(dx1*dx2 .ne. zero)then
              do h = 1,nvar
                 slope1 = ( var_centre(h) - var_left  (h) ) / dx1
                 slope2 = ( var_right (h) - var_centre(h) ) / dx2
                 slope_centre(h) = minmod(slope1,slope2)
              enddo
           endif
           
           ! Right cell
           var_right_right(1) = grid_cells(grid_cells(grid_cells(n)%neighbour(ieven(k)))%neighbour(ieven(k)))%density
           var_right_right(2) = grid_cells(grid_cells(grid_cells(n)%neighbour(ieven(k)))%neighbour(ieven(k)))%momentum(ivar(2,k)-1)
           var_right_right(3) = grid_cells(grid_cells(grid_cells(n)%neighbour(ieven(k)))%neighbour(ieven(k)))%momentum(ivar(3,k)-1)
           var_right_right(4) = grid_cells(grid_cells(grid_cells(n)%neighbour(ieven(k)))%neighbour(ieven(k)))%momentum(ivar(4,k)-1)
           var_right_right(5) = grid_cells(grid_cells(grid_cells(n)%neighbour(ieven(k)))%neighbour(ieven(k)))%energy
           
           dx1 = grid_cells(grid_cells(n)%neighbour(ieven(k)))%x_centre(k)                                 - grid_cells(n)%x_centre(k)
           dx2 = grid_cells(grid_cells(grid_cells(n)%neighbour(ieven(k)))%neighbour(ieven(k)))%x_centre(k) - grid_cells(grid_cells(n)%neighbour(ieven(k)))%x_centre(k)
           if(dx1*dx2 .ne. zero)then
              do h = 1,nvar
                 slope1 = ( var_right      (h) - var_centre(h) ) / dx1
                 slope2 = ( var_right_right(h) - var_right (h) ) / dx2
                 slope_right(h) = minmod(slope1,slope2)
              enddo
           endif
           
        endif
           
        do h = 1,nvar
           state1(h) = var_left  (h) + slope_left  (h) * ( grid_cells(grid_cells(n)%neighbour(iodd(k)))%x_right(k) - grid_cells(grid_cells(n)%neighbour(iodd(k)))%x_centre(k) )
           state2(h) = var_centre(h) - slope_centre(h) * ( grid_cells(n)%x_centre(k)                               - grid_cells(n)%x_left(k)                                  )
           state3(h) = var_centre(h) + slope_centre(h) * ( grid_cells(n)%x_right(k)                                  - grid_cells(n)%x_centre(k)                               )
           state4(h) = var_right (h) - slope_right (h) * ( grid_cells(grid_cells(n)%neighbour(ieven(k)))%x_centre(k) - grid_cells(grid_cells(n)%neighbour(ieven(k)))%x_left(k) )
        enddo
        
        call riemann_solver(state1,state2,flux_left )
        call riemann_solver(state3,state4,flux_right)
        
        do h = 1,nvar
           grid_cells(n)%update(ivar(h,k)) = grid_cells(n)%update(ivar(h,k)) + grid_cells(n)%area(iodd(k))*flux_left(h) - grid_cells(n)%area(ieven(k))*flux_right(h)
        enddo
        
     enddo
     
     ! Store conservative update
     grid_cells(n)%update = grid_cells(n)%update * dt / grid_cells(n)%volume
     
  enddo
  !$OMP END DO
  !$OMP END PARALLEL
  
  return

end subroutine compute_hydro

!###############################################################################

subroutine riemann_solver(ucl,ucr,flux)

  use constants
  use variables

  implicit none
  
  ! Arguments
  real(dp), dimension(nvar     ), intent(in ) :: ucl,ucr
  real(dp), dimension(nvar     ), intent(out) :: flux
  
  ! Local variables
  real(dp), dimension(nvar     ) :: lambda,a,coeff,fl,fr
  real(dp), dimension(nvar,nvar) :: lem,rem
  real(dp)                       :: dl,ul,vl,wl,pl,dr,ur,vr,wr,pr,cs,vsq,norm
  real(dp)                       :: sqrtdl,sqrtdr,vxroe,vyroe,vzroe,hroe
  integer                        :: n,m
  
  dl = ucl(1)
  ul = ucl(2) / ucl(1)
  vl = ucl(3) / ucl(1)
  wl = ucl(4) / ucl(1)
  pl = ( ucl(5) - half*(ucl(2)*ucl(2)+ucl(3)*ucl(3)+ucl(4)*ucl(4))/ucl(1) ) * gm1
  
  dr = ucr(1)
  ur = ucr(2) / ucr(1)
  vr = ucr(3) / ucr(1)
  wr = ucr(4) / ucr(1)
  pr = ( ucr(5) - half*(ucr(2)*ucr(2)+ucr(3)*ucr(3)+ucr(4)*ucr(4))/ucr(1) ) * gm1

  ! Step 1 : Compute Roe-averaged data from left and right states
  !   These averages will be the input variables to the eigen problem
  sqrtdl = sqrt(dl)
  sqrtdr = sqrt(dr)
  vxroe  = (sqrtdl*ul + sqrtdr*ur)/(sqrtdl+sqrtdr)
  vyroe  = (sqrtdl*vl + sqrtdr*vr)/(sqrtdl+sqrtdr)
  vzroe  = (sqrtdl*wl + sqrtdr*wr)/(sqrtdl+sqrtdr)
  hroe   = ((ucl(5)+pl)/sqrtdl+(ucr(5)+pr)/sqrtdr)/(sqrtdl+sqrtdr)

  ! Step 2 : Compute eigenvalues and eigenmatrices from Roe-averaged values
  vsq  = vxroe*vxroe + vyroe*vyroe + vzroe*vzroe
  cs   = sqrt(gm1*max((hroe-half*vsq),epsilon(vsq)))
  norm = half/(cs*cs)

  ! Eigenvalues
  lambda(1) = vxroe - cs
  lambda(2) = vxroe
  lambda(3) = vxroe
  lambda(4) = vxroe
  lambda(5) = vxroe + cs
  
  ! Right eigenmatrix
  rem = reshape((/ one          ,zero ,zero ,one     ,one          , &
                   vxroe-cs     ,zero ,zero ,vxroe   ,vxroe+cs     , &
                   vyroe        ,one  ,zero ,vyroe   ,vyroe        , &
                   vzroe        ,zero ,one  ,vzroe   ,vzroe        , &
                   hroe-vxroe*cs,vyroe,vzroe,half*vsq,hroe+vxroe*cs /), shape(rem))
                             
  ! Left eigenmatrix
  lem = reshape((/ norm*(half*gm1*vsq+vxroe*cs),-norm*(gm1*vxroe+cs),-norm*gm1*vyroe  ,-norm*gm1*vzroe  ,norm*gm1    , &
                   -vyroe                      ,zero                ,one              ,zero             ,zero        , &
                   -vzroe                      ,zero                ,zero             ,one              ,zero        , &
                   one-norm*gm1*vsq            ,gm1*vxroe/(cs*cs)   ,gm1*vyroe/(cs*cs),gm1*vzroe/(cs*cs),-gm1/(cs*cs), &
                   norm*(half*gm1*vsq-vxroe*cs),-norm*(gm1*vxroe-cs),-norm*gm1*vyroe  ,-norm*gm1*vzroe  ,norm*gm1     /), shape(lem))
  
  ! Step 3 : Create intermediate states from eigenmatrices
  a(:) = (ucr(1)-ucl(1))*lem(1,:)
  do n = 2,nvar
     a(:) = a(:) + (ucr(n)-ucl(n))*lem(n,:)
  enddo

  ! Step 4 : Compute L/R fluxes
  fl(1) = ucl(2)         ; fr(1) = ucr(2)
  fl(2) = ucl(2)*ul + pl ; fr(2) = ucr(2)*ur + pr
  fl(3) = ucl(2)*vl      ; fr(3) = ucr(2)*vr
  fl(4) = ucl(2)*wl      ; fr(4) = ucr(2)*wr
  fl(5) = (ucl(5)+pl)*ul ; fr(5) = (ucr(5)+pr)*ur

  ! Step 5 : Compute Roe flux
  do n = 1,nvar
     flux (n) = half*(fl(n)+fr(n))
     coeff(n) = half*abs(lambda(n))*a(n)
  enddo

  do n = 1,nvar
     do m = 1,nvar
        flux(m) = flux(m) - coeff(n)*rem(n,m)
     enddo
  enddo

  return

end subroutine riemann_solver

!###############################################################################

function nmod(i,j)

  implicit none
  
  integer, intent(in) :: i,j
  integer             :: nmod
  
  nmod = mod(i,j)
  
  if(nmod == 0) nmod = j

end function nmod

!###############################################################################

function minmod(a,b)

  use constants
  
  implicit none
  
  real(dp), intent(in) :: a,b
  real(dp)             :: minmod
  
  if(abs(a) > abs(b))then
     minmod = b
  else
     minmod = a
  endif
  if(a*b < zero)then
     minmod = zero
  endif
  
end function minmod

!###############################################################################

subroutine generate_colormap

  use variables
  
  implicit none
  
  real(dp) :: gradient,offset
  integer  :: i

  ! Create colormap
  ncolors = 255
  allocate(red(ncolors),green(ncolors),blue(ncolors))
  gradient = 0.0103_dp ; offset = 0.0318_dp
  do i = 1,ncolors
     red  (i) = min(max(nint(ncolors*(gradient*real(i-1,dp) + offset       )),0),ncolors)
     green(i) = min(max(nint(ncolors*(gradient*real(i-1,dp) + offset-1.0_dp)),0),ncolors)
     blue (i) = min(max(nint(ncolors*(gradient*real(i-1,dp) + offset-2.0_dp)),0),ncolors)
  enddo
  
  return
  
end subroutine generate_colormap
