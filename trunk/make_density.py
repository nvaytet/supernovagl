from pylab import *

#ff = open('colormap.js','w')

nimx = 128
nimy = 72

#map_list = ["jet","hot","gnuplot","RdBu_r","RdGy_r"]
map_list = ["galaxy","solarsystem","planet","satellite","rocket","blackhole","invader","deathstar","meteor","constellation","lepetitprince"]

ff = open('load_density.js','w')
ff.write("function updateDensity() {\n")
ff.write("    var nimx = %i;\n" % (nimx))
ff.write("    var nimy = %i;\n" % (nimy))
ff.write("    var image = new Array(nimx);\n")
ff.write("    for (var i = 0; i < nimx; i++) {\n")
ff.write("        image[i] = new Array(nimy);\n")
ff.write("    }\n")
#ff.write("    r   = [];\n")
#ff.write("    green = [];\n")
#ff.write("    blue  = [];\n")
#ff.write("    red   = new Array(%i);\n" % (ncolors))
#ff.write("    green = new Array(%i);\n" % (ncolors))
#ff.write("    blue  = new Array(%i);\n" % (ncolors))

ff.write("    mymap = document.getElementById(\"selectDensity\").value;\n")

ff.write("    switch(mymap) {\n")


nmaps = shape(map_list)[0]

    
for n in range(nmaps):
    
    ff.write("    case \""+map_list[n]+"\" :\n")
    
    f1 = open("../stuff/"+map_list[n]+"_128.ppm",'r')
    dum = f1.readline()
    dum = f1.readline()
    dum = f1.readline()
    dum = f1.readline()
    
    for j in range(nimy):
        for i in range(nimx):
            rd = int(f1.readline())
            gn = int(f1.readline())
            bl = int(f1.readline())
            #print rd,gn,bl
            ff.write("        image[%3i][%3i] = %f;\n" % (i,j,(rd+gn+bl)/765.0))
    
    ff.write("        break;\n")
    f1.close()

ff.write("    }\n")

ff.write("    var dxim = nx/nimx;\n")
ff.write("    var dyim = ny/nimy;\n")
ff.write("    var real_i;\n")
ff.write("    var real_j;\n")
ff.write("    var int_i;\n")
ff.write("    var int_j;\n")

ff.write("    for (var i = nx1-1; i < nx2; i++) {\n")
ff.write("        for (var j = ny1-1; j < ny2; j++) {\n")
ff.write("            real_i = (i-nghosts)/dxim;\n")
ff.write("            real_j = (j-nghosts)/dyim;\n")
ff.write("            int_i = parseInt(real_i);\n")
ff.write("            int_j = parseInt(real_j);\n")
ff.write("            dens_old[i][j] = d1 + d2 * image[int_i][int_j];\n")
#ff.write("            dens_new[i][j] = d1 + d2 * image[iim][jim];\n")
ff.write("        }\n")
ff.write("    }\n")
ff.write("    resetCanvas();\n")

ff.write("};\n")

ff.close



