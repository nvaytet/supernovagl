//###############################################################################
// Simulation setup
//###############################################################################

// function resetCanvas() {
// 
//     for (var i = 0; i < nnx; i++) {
//         for (var j = 0; j < nny; j++) {
//             
//             // Conservative variables
//             // Density is set in separate scripts which loads an image
//             momx_old[i][j] = 0.0;
//             momy_old[i][j] = 0.0;
//             ener_old[i][j] = p1 / gm1;
//                  
//             dens_new[i][j] = dens_old[i][j];
//             momx_new[i][j] = momx_old[i][j];
//             momy_new[i][j] = momy_old[i][j];
//             ener_new[i][j] = ener_old[i][j];
//             
//             update_count[i][j] = 0;
//         }
//     }
//     
// };
// 
// // resetCanvas();
// 
// Compute initial timestep
compute_timestep();
// 
// 
// 
// 
// // Add click event listener on the canvas
// var theCanvas = document.getElementById('myCanvas');
// theCanvas.addEventListener("click", getClickPosition, false);
// // theCanvas.addEventListener( "contextmenu", blackHole, false);
// 
// function getClickPosition(e) {
// //     var rect = theCanvas.getBoundingClientRect();
// //       x: evt.clientX - rect.left,
// //       y: evt.clientY - rect.top
// //     var xPosition = Math.round((e.clientX-rect.left)/(rect.right -rect.left)*theCanvas.width )/celldx;
// //     var yPosition = Math.round((e.clientY-rect.top )/(rect.bottom-rect.top )*theCanvas.height)/celldy;
// //     alert(e.which);
//     icell = Math.round(e.clientX/celldx);
//     jcell = Math.round(e.clientY/celldy);
//     
//     switch(e.which) {
//         case 1:
//             // Call energy injection
//             inject_energy();
//             break;
//         case 2:
//             add_new_grav_source = true;
//             break;
//     }
// };

// function blackHole(e) {
//     icell = Math.round(e.clientX/celldx);
//     jcell = Math.round(e.clientY/celldy);
//     
//     add_new_grav_source = true;
// };



// // poor man's solution
// var ONE_FRAME_TIME = 5;
// var mainloop = function() {
//     compute_hydro();
//     if (it % nout == 0) {
//        update_screen();
//     }
// };
// setInterval( mainloop, ONE_FRAME_TIME );


// Create animation loop
var mainloop = function() {
    it = it + 1; // Increase timestep number
    compute_hydro();
    if (it > nout) {
       update_screen();
       it = 0;
    }
};

var animFrame = window.requestAnimationFrame       ||
                window.webkitRequestAnimationFrame ||
                window.mozRequestAnimationFrame    ||
                window.oRequestAnimationFrame      ||
                window.msRequestAnimationFrame     ||
                null ;

var recursiveAnim = function() {
    mainloop();
    animFrame( recursiveAnim );
};

// start the mainloop
animFrame( recursiveAnim );


// Inject energy when mouse left button is clicked.
// This is smoothed over a small region using a gaussian kernel
function inject_energy() {
    
    var i1 = Math.max(icell-kw,nx1);
    var i2 = Math.min(icell+kw,nx2);
    var j1 = Math.max(jcell-kw,ny1);
    var j2 = Math.min(jcell+kw,ny2);

    for (var i = i1; i < i2+1; i++) {
        for (var j = j1; j < j2+1; j++) {
            
            ener_old[i][j] = ener_old[i][j] + p2 * gkernel[i-icell+kw][j-jcell+kw] / gm1;
            ener_new[i][j] = ener_old[i][j];
            
            dtcell = dt_cell(i,j);
            dtnew = Math.min(dtnew,dtcell);
            
        }
    }
    
};

// // Inject energy when mouse left button is clicked.
// // This is smoothed over a small region using a gaussian kernel
// function inject_energy() {
//     
//     add_new_grav_source = true;
//     
// //     ngrav = ngrav + 1;
// //     gravity_x[ngrav-1] = icell;
// //     gravity_y[ngrav-1] = jcell;
// //     
// //     var i1 = Math.max(icell-kw,nx1);
// //     var i2 = Math.min(icell+kw,nx2);
// //     var j1 = Math.max(jcell-kw,ny1);
// //     var j2 = Math.min(jcell+kw,ny2);
// // 
// //     for (var i = i1; i < i2+1; i++) {
// //         for (var j = j1; j < j2+1; j++) {
// //             
// //             dens_old[i][j] = (1.0 - gkernel[i-icell+kw][j-jcell+kw]) * dens_old[i][j] + gkernel[i-icell+kw][j-jcell+kw] * dfix;
// //             dens_new[i][j] = dens_old[i][j];
// //             
// //             dtcell = dt_cell(i,j);
// //             dtnew = Math.min(dtnew,dtcell);
// //             
// //         }
// //     }
// //     
// //     alert("in inject energy"+String(icell)+" "+String(jcell)+" "+String(gravity_x[k-1]))
//     
// };


//###############################################################################
// Classical hydrodynamics routines
//###############################################################################

// Compute initial timestep
function compute_timestep() {

    var soundspeed;
    var wsx;
    var wsy;
    var dtx;
    var dty;
    var kinetic_energy;
    var internal_energy;
    
    dt = large_number;
    

    for (var i = nx1-1; i < nx2; i++) {
        for (var j = ny1-1; j < ny2; j++) {
            
            // Compute sound speed inside cell
            kinetic_energy = 0.5 * (momx_new[i][j]*momx_new[i][j] + momy_new[i][j]*momy_new[i][j]) / dens_new[i][j];
            
            internal_energy = ener_new[i][j] - kinetic_energy;
         
            soundspeed = Math.sqrt( gam * gm1 * internal_energy / dens_new[i][j] );
         
            wsx = Math.abs(momx_new[i][j])/dens_new[i][j] + soundspeed;
            wsy = Math.abs(momy_new[i][j])/dens_new[i][j] + soundspeed;
            
            dtx = 1.0 / wsx;
            dty = 1.0 / wsy;
         
            dt = Math.min(dt,dtx,dty);
            
        }
    }
          
  // CFL condition
  dt = cfl * dt
  
};

// Compute timestep in given cell
// Also perform a pressure fix at the same time to keep the number of loops to a minimum
function dt_cell(ii,jj) {
    
    // Pressure fix
    var density = dens_new[ii][jj]
    var kinetic_energy = 0.5 * (momx_new[ii][jj]*momx_new[ii][jj] + momy_new[ii][jj]*momy_new[ii][jj]) / density;
    var internal_energy = ener_new[ii][jj] - kinetic_energy;

    if(density < dfix){
        // maintain the same velocity and pressure in cell whilst resetting density
        dens_new[ii][jj] = dfix
        momx_new[ii][jj] = momx_new[ii][jj] / density * dfix
        momy_new[ii][jj] = momy_new[ii][jj] / density * dfix
        kinetic_energy = 0.5 * (momx_new[ii][jj]*momx_new[ii][jj] + momy_new[ii][jj]*momy_new[ii][jj]) / dfix;
        density = dfix
    }
    
    // internal energy
    if(internal_energy < pfix*kinetic_energy){
        internal_energy = pfix * kinetic_energy
    }

    // update total energy
    ener_new[ii][jj] = internal_energy + kinetic_energy
    
    // Compute sound speed inside cell     
    var soundspeed = Math.sqrt( gam * gm1 * internal_energy / density );
 
    // Wave speeds
    var wsx = Math.abs(momx_new[ii][jj])/density + soundspeed;
    var wsy = Math.abs(momy_new[ii][jj])/density + soundspeed;
    
    var dtx = cfl / wsx;
    var dty = cfl / wsy;
 
    var dtc = Math.min(dtx,dty);
          
    // CFL condition
//     dtc = cfl * dtc
  
    return dtc;
  
};

//###############################################################################

// Main hydro loop to compute fluxes and timestep in the entire grid
function compute_hydro() {
    
    var ig,jg;
    var density,kinetic_energy,internal_energy;

    
//     it = it + 1; // Increase timestep number
    dtnew = large_number;
    
    // fill ghosts cells
    for (var j = ny1-1; j < ny2; j++) {
        for (var i = 0; i < nghosts; i++) {
            dens_old[i][j] = dens_old[nx1-1][j];
            momx_old[i][j] = momx_old[nx1-1][j];
            momy_old[i][j] = momy_old[nx1-1][j];
            ener_old[i][j] = ener_old[nx1-1][j];
        }
        for (var i = nx2; i < nnx; i++) {
            dens_old[i][j] = dens_old[nx2-1][j];
            momx_old[i][j] = momx_old[nx2-1][j];
            momy_old[i][j] = momy_old[nx2-1][j];
            ener_old[i][j] = ener_old[nx2-1][j];
        }
    }
    
    for (var i = nx1-1; i < nx2; i++) {
        for (var j = 0; j < nghosts; j++) {
            dens_old[i][j] = dens_old[i][ny1-1];
            momx_old[i][j] = momx_old[i][ny1-1];
            momy_old[i][j] = momy_old[i][ny1-1];
            ener_old[i][j] = ener_old[i][ny1-1];
        }
        for (var j = ny2; j < nny; j++) {
            dens_old[i][j] = dens_old[i][ny2-1];
            momx_old[i][j] = momx_old[i][ny2-1];
            momy_old[i][j] = momy_old[i][ny2-1];
            ener_old[i][j] = ener_old[i][ny2-1];
        }
    }
    
    
    for (var i = nx1-1; i < nx2+1; i++) {
        for (var j = ny1-1; j < ny2+1; j++) {
         
            // Compute values at the cell interfaces using gradients
            compute_interface_values([dens_old[i-2][j],dens_old[i-1][j],dens_old[i][j],dens_old[i+1][j]],[momx_old[i-2][j],momx_old[i-1][j],momx_old[i][j],momx_old[i+1][j]],[momy_old[i-2][j],momy_old[i-1][j],momy_old[i][j],momy_old[i+1][j]],[ener_old[i-2][j],ener_old[i-1][j],ener_old[i][j],ener_old[i+1][j]],varxl,varxr)
            
            compute_interface_values([dens_old[i][j-2],dens_old[i][j-1],dens_old[i][j],dens_old[i][j+1]],[momy_old[i][j-2],momy_old[i][j-1],momy_old[i][j],momy_old[i][j+1]],[momx_old[i][j-2],momx_old[i][j-1],momx_old[i][j],momx_old[i][j+1]],[ener_old[i][j-2],ener_old[i][j-1],ener_old[i][j],ener_old[i][j+1]],varyl,varyr)
            
            // Compute fluxes using Riemann solver
            riemann_solver(varxl,varxr,fluxx);
            riemann_solver(varyl,varyr,fluxy);
            
            // Update conservative variables on each side of the interface
            dens_new[i  ][j] = dens_new[i  ][j] + fluxx[0]*dt;
            dens_new[i-1][j] = dens_new[i-1][j] - fluxx[0]*dt;
            momx_new[i  ][j] = momx_new[i  ][j] + fluxx[1]*dt;
            momx_new[i-1][j] = momx_new[i-1][j] - fluxx[1]*dt;
            momy_new[i  ][j] = momy_new[i  ][j] + fluxx[2]*dt;
            momy_new[i-1][j] = momy_new[i-1][j] - fluxx[2]*dt;
            ener_new[i  ][j] = ener_new[i  ][j] + fluxx[3]*dt;
            ener_new[i-1][j] = ener_new[i-1][j] - fluxx[3]*dt;
            
            dens_new[i][j  ] = dens_new[i][j  ] + fluxy[0]*dt;
            dens_new[i][j-1] = dens_new[i][j-1] - fluxy[0]*dt;
            momy_new[i][j  ] = momy_new[i][j  ] + fluxy[1]*dt;
            momy_new[i][j-1] = momy_new[i][j-1] - fluxy[1]*dt;
            momx_new[i][j  ] = momx_new[i][j  ] + fluxy[2]*dt;
            momx_new[i][j-1] = momx_new[i][j-1] - fluxy[2]*dt;
            ener_new[i][j  ] = ener_new[i][j  ] + fluxy[3]*dt;
            ener_new[i][j-1] = ener_new[i][j-1] - fluxy[3]*dt;
            
            // Count how many faces have been updated
            update_count[i  ][j] = update_count[i  ][j] + 2;
            update_count[i-1][j] = update_count[i-1][j] + 1;
            update_count[i][j-1] = update_count[i][j-1] + 1;
            
            // If all faces have been updates, the cell will not be revisited, so we can
            // finalise the update and compute the new timestep.
            // we only need to check in i-1, not j-1 because of the order of the i,j loops
            if(update_count[i-1][j] == 4){
                
                // Add gravity source
                if (ngrav > 0) {
                    fsum1 = 0.0;
                    fsum2 = 0.0;
                    for (var k = 0; k < ngrav; k++) {
                        dist1  = i-1 - gravity_x[k];
                        dist2  = j   - gravity_y[k];
                        distsq = dist1*dist1 + dist2*dist2;
                        if (distsq > 0) {
//                             dist   = Math.sqrt(distsq);
                            fgrav = - grav / distsq;
                            th = Math.atan2(dist2,dist1)
                            fsum1 = fsum1 + fgrav * Math.cos(th);
                            fsum2 = fsum2 + fgrav * Math.sin(th);
//                             th = 0.5*pi;
//                             if (dist2 > 0.0) {
//                                 ph = Math.acos(dist1/dist*Math.sin(th));
//                             } else if (dist2 < 0.0) {
//                                 ph = Math.acos(-dist1/dist*Math.sin(th)) + pi;
//                             } else {
//                                 ph = 2.0*pi + Math.asin(dist2/dist*Math.sin(th));
//                             }
//                             fsum1 = fsum1 + fgrav * Math.sin(th) * Math.cos(ph);
//                             fsum2 = fsum2 + fgrav * Math.sin(th) * Math.sin(ph);
                        }
                    }
                    
                    momx_new[i-1][j] = momx_new[i-1][j] + dens_old[i-1][j]*fsum1*dt;
                    momy_new[i-1][j] = momy_new[i-1][j] + dens_old[i-1][j]*fsum2*dt;
                    ener_new[i-1][j] = ener_new[i-1][j] + (momx_old[i-1][j]*fsum1 + momy_old[i-1][j]*fsum2)*dt;
                }
                
                dtcell = dt_cell(i-1,j);
                dtnew = Math.min(dtnew,dtcell);
                
                mass_diff[i-1][j] = dens_new[i-1][j] - dens_old[i-1][j];
                
                dens_old[i-1][j] = dens_new[i-1][j];
                momx_old[i-1][j] = momx_new[i-1][j];
                momy_old[i-1][j] = momy_new[i-1][j];
                ener_old[i-1][j] = ener_new[i-1][j];
                
                update_count[i-1][j] = 0;
                
            }
            
        }
    }
    
//     time = time + dt;
    
    dt = dtnew;
    
    if (add_new_grav_source) {
        ngrav = ngrav + 1;
        gravity_x[ngrav-1] = icell;
        gravity_y[ngrav-1] = jcell;
//         gravity_z[ngrav-1] = 1.0;
        gravity_z[ngrav-1] = dens_old[icell][jcell];
        
        
//         var i1 = Math.max(icell-kw,nx1);
//         var i2 = Math.min(icell+kw,nx2);
//         var j1 = Math.max(jcell-kw,ny1);
//         var j2 = Math.min(jcell+kw,ny2);
// 
//         for (var i = i1; i < i2+1; i++) {
//             for (var j = j1; j < j2+1; j++) {
//                 
//                 dens_old[i][j] = (1.0 - gkernel[i-icell+kw][j-jcell+kw]) * dens_old[i][j] + gkernel[i-icell+kw][j-jcell+kw] * dfix;
//                 dens_new[i][j] = dens_old[i][j];
//                 
// //                 dtcell = dt_cell(i,j);
// //                 dtnew = Math.min(dtnew,dtcell);
//                 
//             }
//         }
        add_new_grav_source = false;
    }

    // Keep density constant like a sink
    if (ngrav > 0) {
        for (var k = 0; k < ngrav; k++) {
            ig = gravity_x[k];
            jg = gravity_y[k]
            density = dens_old[ig][jg];
            kinetic_energy = 0.5 * (momx_old[ig][jg]*momx_old[ig][jg] + momy_old[ig][jg]*momy_old[ig][jg]) / density;
            internal_energy = ener_old[ig][jg] - kinetic_energy;
            dens_old[ig][jg] = gravity_z[k];
            
            momx_old[ig][jg] = momx_old[ig][jg] / density * gravity_z[k]
            momy_old[ig][jg] = momy_old[ig][jg] / density * gravity_z[k]
            kinetic_energy = 0.5 * (momx_old[ig][jg]*momx_old[ig][jg] + momy_old[ig][jg]*momy_old[ig][jg]) / gravity_z[k];
            // update total energy
            ener_old[ig][jg] = internal_energy + kinetic_energy
            
//             gravity_z[k] = gravity_z[k] + mass_diff[gravity_x[k]][gravity_y[k]];
        }
    }

};
     
//###############################################################################

function riemann_solver(ucl,ucr,flux) {

    var sl,sr;
    var dl,pl,ul,vl,el;
    var dr,pr,ur,vr,er;
    var cfastl,rcl,dstarl;
    var cfastr,rcr,dstarr;
    var estarl,estarr;
    var ustar,pstar;
    var dd,uu,pp,ee;
    
    dl = ucl[0]
    ul = ucl[1] / ucl[0]
    vl = ucl[2] / ucl[0]
    el = ucl[3]
    pl = ( ucl[3] - 0.5*(ucl[1]*ucl[1]+ucl[2]*ucl[2])/ucl[0] ) * gm1
    
    dr = ucr[0]
    ur = ucr[1] / ucr[0]
    vr = ucr[2] / ucr[0]
    er = ucr[3]
    pr = ( ucr[3] - 0.5*(ucr[1]*ucr[1]+ucr[2]*ucr[2])/ucr[0] ) * gm1
    
    
    // Find the largest eigenvalues in the normal direction to the interface
    cfastl = Math.sqrt(gam*pl/dl)
    cfastr = Math.sqrt(gam*pr/dr)
    
    // Compute HLL wave speed
    sl = Math.min(ul,ur) - Math.max(cfastl,cfastr)
    sr = Math.max(ul,ur) + Math.max(cfastl,cfastr)
    
    // Compute lagrangian sound speed
    rcl = dl * (ul - sl)
    rcr = dr * (sr - ur)
    
    // Compute acoustic star state
    ustar = (rcr*ur   +rcl*ul   +  (pl-pr))/(rcr+rcl)
    pstar = (rcr*pl+rcl*pr+rcl*rcr*(ul-ur))/(rcr+rcl)
    
    // Left star region variables
    dstarl = dl*(sl-ul)/(sl-ustar)
    estarl = ((sl-ul)*el-pl*ul+pstar*ustar)/(sl-ustar)
    
    // Right star region variables
    dstarr = dr*(sr-ur)/(sr-ustar)
    estarr = ((sr-ur)*er-pr*ur+pstar*ustar)/(sr-ustar)
    
    // Sample the solution at x/t=0
    if (sl > 0.0) {
        dd = dl;
        uu = ul;
        pp = pl;
        ee = el;
    } else if (ustar > 0.0) {
        dd = dstarl;
        uu = ustar;
        pp = pstar;
        ee = estarl;
    } else if (sr > 0.0) {
        dd = dstarr;
        uu = ustar;
        pp = pstar;
        ee = estarr;
    } else {
        dd = dr;
        uu = ur;
        pp = pr;
        ee = er;
    }
    
    // Compute the Godunov flux
    flux[0] = dd*uu;
    flux[1] = dd*uu*uu + pp;
    flux[3] = (ee + pp) * uu;
    if (flux[0] > 0.0) {
        flux[2] = dd*uu*vl;
    } else {
        flux[2] = dd*uu*vr;
    }
  
};



// Compute interface values with slopes
function compute_interface_values(dens,momx,momy,ener,varl,varr) {

    var dlft;
    var drgt;
    var dcen;
    var dsgn;
    var dlim;
    var av;
    
    // Left state
    
    dlft = 2.0*(dens[1]-dens[0]);
    drgt = 2.0*(dens[2]-dens[1]);
    dcen = 0.5*(dens[2]-dens[0]);
    dsgn = Math.sign(dcen);
    dlim = Math.min(Math.abs(dlft),Math.abs(drgt));
    if((dlft*drgt) <= 0.0){
        dlim = 0.0;
    }
    av = 0.5*dsgn*Math.min(dlim,Math.abs(dcen));
    varl[0] = dens[1] + av;
    
    dlft = 2.0*(momx[1]-momx[0]);
    drgt = 2.0*(momx[2]-momx[1]);
    dcen = 0.5*(momx[2]-momx[0]);
    dsgn = Math.sign(dcen);
    dlim = Math.min(Math.abs(dlft),Math.abs(drgt));
    if((dlft*drgt) <= 0.0){
        dlim = 0.0;
    }
    av = 0.5*dsgn*Math.min(dlim,Math.abs(dcen));
    varl[1] = momx[1] + av;
    
    dlft = 2.0*(momy[1]-momy[0]);
    drgt = 2.0*(momy[2]-momy[1]);
    dcen = 0.5*(momy[2]-momy[0]);
    dsgn = Math.sign(dcen);
    dlim = Math.min(Math.abs(dlft),Math.abs(drgt));
    if((dlft*drgt) <= 0.0){
        dlim = 0.0;
    }
    av = 0.5*dsgn*Math.min(dlim,Math.abs(dcen));
    varl[2] = momy[1] + av;
    
    dlft = 2.0*(ener[1]-ener[0]);
    drgt = 2.0*(ener[2]-ener[1]);
    dcen = 0.5*(ener[2]-ener[0]);
    dsgn = Math.sign(dcen);
    dlim = Math.min(Math.abs(dlft),Math.abs(drgt));
    if((dlft*drgt) <= 0.0){
        dlim = 0.0;
    }
    av = 0.5*dsgn*Math.min(dlim,Math.abs(dcen));
    varl[3] = ener[1] + av;
    
    
    // Right state
    
    dlft = 2.0*(dens[2]-dens[1]);
    drgt = 2.0*(dens[3]-dens[2]);
    dcen = 0.5*(dens[3]-dens[1]);
    dsgn = Math.sign(dcen);
    dlim = Math.min(Math.abs(dlft),Math.abs(drgt));
    if((dlft*drgt) <= 0.0){
        dlim = 0.0;
    }
    av = 0.5*dsgn*Math.min(dlim,Math.abs(dcen));
    varr[0] = dens[2] - av;
    
    dlft = 2.0*(momx[2]-momx[1]);
    drgt = 2.0*(momx[3]-momx[2]);
    dcen = 0.5*(momx[3]-momx[1]);
    dsgn = Math.sign(dcen);
    dlim = Math.min(Math.abs(dlft),Math.abs(drgt));
    if((dlft*drgt) <= 0.0){
        dlim = 0.0;
    }
    av = 0.5*dsgn*Math.min(dlim,Math.abs(dcen));
    varr[1] = momx[2] - av;
    
    dlft = 2.0*(momy[2]-momy[1]);
    drgt = 2.0*(momy[3]-momy[2]);
    dcen = 0.5*(momy[3]-momy[1]);
    dsgn = Math.sign(dcen);
    dlim = Math.min(Math.abs(dlft),Math.abs(drgt));
    if((dlft*drgt) <= 0.0){
        dlim = 0.0;
    }
    av = 0.5*dsgn*Math.min(dlim,Math.abs(dcen));
    varr[2] = momy[2] - av;
    
    dlft = 2.0*(ener[2]-ener[1]);
    drgt = 2.0*(ener[3]-ener[2]);
    dcen = 0.5*(ener[3]-ener[1]);
    dsgn = Math.sign(dcen);
    dlim = Math.min(Math.abs(dlft),Math.abs(drgt));
    if((dlft*drgt) <= 0.0){
        dlim = 0.0;
    }
    av = 0.5*dsgn*Math.min(dlim,Math.abs(dcen));
    varr[3] = ener[2] - av;
    
    
    
};




//###############################################################################
// Update graphics
//###############################################################################

function update_screen() {

    var canvas = document.getElementById("myCanvas");
    var ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, canvas.width, canvas.height); // Clear the canvas and start fresh

    var icol;
  
    for (var i = nx1-1; i < nx2; i++) {
        for (var j = ny1-1; j < ny2; j++) {
            
            icol = Math.min(Math.max(Math.floor(((dens_new[i][j]-dmin)/(dmax-dmin))*(ncolors-1)),0),ncolors-1)
            theString = "rgb("+String(red[icol])+","+String(green[icol])+","+String(blue[icol])+")";
            ctx.fillStyle = theString;
            ctx.fillRect(i*celldx,j*celldy,celldx,celldy);
        }
    }
    
};
