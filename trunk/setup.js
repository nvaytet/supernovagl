
var windx = 1000;
var windy = 563;
var ratio = windy / windx

// Declare variables
var nvar = 4;
var ndim = 2;
var nx = 128;
var ny = Math.floor(nx * ratio);
var nghosts = 2;
var nnx = nx + 2*nghosts;
var nny = ny + 2*nghosts;
var nx1 = nghosts + 1;
var nx2 = nghosts + nx;
var ny1 = nghosts + 1;
var ny2 = nghosts + ny;
// var time = 0.0;
var dt;
var it = 0;
var gam = 1.4;
var gm1 = gam - 1.0;
var grav = 1.0;
// var time_limit = 10.0;
var cfl = 0.4;
var ncolors = 255;
var large_number = 1.00e+30;
var nout = 1;
var dmin       = 0.2;
var dmax       = 2.5;
var dtnew;
var dfix = 0.1;
var pfix = 1.0e-10;
var icell = 1;
var jcell = 1;

var d1 =  1.0;
var d2 =  2.0;
var p1 =  1.0;
var p2 = 1000.0;


// Gravity
var fsum1;
var fsum2;
var dist1;
var dist2;
var distsq;
var dist;
var fgrav;
var th;
var ph;
var ngrav = 0;
var gravity_x = new Array(100);
var gravity_y = new Array(100);
var gravity_z = new Array(100);
var add_new_grav_source = false;
var pi = 3.141592653


// Arrays

var dens_old = new Array(nnx);
for (var i = 0; i < nnx; i++) {
  dens_old[i] = new Array(nny);
}

var dens_new = new Array(nnx);
for (var i = 0; i < nnx; i++) {
  dens_new[i] = new Array(nny);
}

var momx_old = new Array(nnx);
for (var i = 0; i < nnx; i++) {
  momx_old[i] = new Array(nny);
}

var momx_new = new Array(nnx);
for (var i = 0; i < nnx; i++) {
  momx_new[i] = new Array(nny);
}

var momy_old = new Array(nnx);
for (var i = 0; i < nnx; i++) {
  momy_old[i] = new Array(nny);
}

var momy_new = new Array(nnx);
for (var i = 0; i < nnx; i++) {
  momy_new[i] = new Array(nny);
}

var ener_old = new Array(nnx);
for (var i = 0; i < nnx; i++) {
  ener_old[i] = new Array(nny);
}

var ener_new = new Array(nnx);
for (var i = 0; i < nnx; i++) {
  ener_new[i] = new Array(nny);
}

var update_count = new Array(nnx);
for (var i = 0; i < nnx; i++) {
  update_count[i] = new Array(nny);
}

var mass_diff = new Array(nnx);
for (var i = 0; i < nnx; i++) {
  mass_diff[i] = new Array(nny);
}


var red   = new Array(ncolors);
var green = new Array(ncolors);
var blue  = new Array(ncolors);

var varxl = new Array(nvar);
var varxr = new Array(nvar);
var varyl = new Array(nvar);
var varyr = new Array(nvar);
var fluxx = new Array(nvar);
var fluxy = new Array(nvar);

// Gaussian kernel
var kw = 20;
var ngk = 2*kw + 1;
var gkernel = new Array(ngk);
for (var i = 0; i < ngk; i++) {
  gkernel[i] = new Array(ngk);
}
var x = 0.0;
var gsum = 0.0;
var x0 = (ngk+1)*0.5;
var y  = x0*x0/(16.0*Math.log(2.0));
for (var i = 0; i < ngk; i++) {
    for (var j = 0; j < ngk; j++) {
        x = ((i+1)-x0)*((i+1)-x0) + ((j+1)-x0)*((j+1)-x0);
        gkernel[i][j] = Math.exp(-x/y);
        gsum = gsum + gkernel[i][j];
    }
}
for (var i = 0; i < ngk; i++) {
    for (var j = 0; j < ngk; j++) {
       gkernel[i][j] = gkernel[i][j]/gsum;
    }
}


// Make sure all values of density are set in array
for (var i = 0; i < nnx; i++) {
    for (var j = 0; j < nny; j++) {
        // Conservative variables
        dens_old[i][j] = d1;
    }
}

//###############################################################################
// Generate canvas
//###############################################################################

// Determine cell sizes in pixels
var celldx = windx / nx;
var celldy = windy / ny;
var cellid;

document.write("<table margin=\"0\">");
document.write("<tr>");
document.write("<td>");
document.write("<canvas onmousedown=\"getClickPosition(event)\" id=\"myCanvas\" width=\""+windx+"\" height=\""+windy+"\" style=\"border:0px;margin:0px;padding:0px;\">Your browser does not support the canvas element.</canvas>");
document.write("</td>");
document.write("<td>");
document.write("Select color scheme:<br>");
document.write("<select id=\"selectColormap\" onchange=\"updateColormap()\">");
document.write("    <option selected value=\"Greens\">Greens</option>");
document.write("    <option value=\"hot\">hot</option>");
document.write("    <option value=\"jet\">jet</option>");
document.write("    <option value=\"gnuplot\">space</option>");
document.write("    <option value=\"RdBu_r\">red-blue</option>");
document.write("    <option value=\"RdGy_r\">red-grey</option>");
document.write("    <option value=\"Greys\">black&amp;white</option>");
document.write("</select>");
document.write("<br>");
document.write("<br>");
document.write("<br>");
document.write("Select universe:<br>");
document.write("<select id=\"selectDensity\" onchange=\"updateDensity()\">");
document.write("    <option selected value=\"artis\">Artis</option>");
document.write("    <option value=\"galaxy\">galaxy</option>");
document.write("    <option value=\"solarsystem\">solarsystem</option>");
document.write("    <option value=\"planet\">planet</option>");
document.write("    <option value=\"satellite\">satellite</option>");
document.write("    <option value=\"rocket\">rocket</option>");
document.write("    <option value=\"blackhole\">blackhole</option>");
document.write("    <option value=\"invader\">invader</option>");
document.write("    <option value=\"deathstar\">deathstar</option>");
document.write("    <option value=\"meteor\">meteor</option>");
document.write("    <option value=\"constellation\">constellation</option>");
document.write("    <option value=\"lepetitprince\">lepetitprince</option>");
document.write("</select>");
document.write("<br>");
document.write("<br>");
document.write("<br>");
document.write("<button onclick=\"window.location.reload(true)\">Reset</button>");
document.write("</td>");
document.write("</tr>");
document.write("</table>");
document.write("Left click for supernova explosion, middle click for black hole gravitational field.");



//###############################################################################
// Generate colormap
//###############################################################################


function resetCanvas() {

    for (var i = 0; i < nnx; i++) {
        for (var j = 0; j < nny; j++) {
            
            // Conservative variables
            // Density is set in separate scripts which loads an image
            momx_old[i][j] = 0.0;
            momy_old[i][j] = 0.0;
            ener_old[i][j] = p1 / gm1;
                 
            dens_new[i][j] = dens_old[i][j];
            momx_new[i][j] = momx_old[i][j];
            momy_new[i][j] = momy_old[i][j];
            ener_new[i][j] = ener_old[i][j];
            
            update_count[i][j] = 0;
        }
    }
    
    ngrav = 0;
    
};


updateColormap();
updateDensity();







// resetCanvas();

// Compute initial timestep
// compute_timestep();




// // Add click event listener on the canvas
// var theCanvas = document.getElementById('myCanvas');
// theCanvas.addEventListener("click", getClickPosition, false);
// // theCanvas.addEventListener( "contextmenu", blackHole, false);

function getClickPosition(e) {
//     var rect = theCanvas.getBoundingClientRect();
//       x: evt.clientX - rect.left,
//       y: evt.clientY - rect.top
//     var xPosition = Math.round((e.clientX-rect.left)/(rect.right -rect.left)*theCanvas.width )/celldx;
//     var yPosition = Math.round((e.clientY-rect.top )/(rect.bottom-rect.top )*theCanvas.height)/celldy;
//     alert(e.which);
    icell = Math.round(e.clientX/celldx);
    jcell = Math.round(e.clientY/celldy);
    
//     switch(e.which) {
//         case 1:
//             // Call energy injection
//             inject_energy();
//             break;
//         case 2:
//             add_new_grav_source = true;
//             break;
//     }
    
    switch(e.button) {
        case 0:
            // Call energy injection
            inject_energy();
            break;
        case 1:
            add_new_grav_source = true;
            break;
    }
};




// var gradient = 0.0103;
// var offset = 0.0318;
// for (var i = 0; i < ncolors; i++) {
//     red[i]   = Math.min(Math.max(Math.floor(ncolors*(gradient*(i-1) + offset    )),0),ncolors)
//     green[i] = Math.min(Math.max(Math.floor(ncolors*(gradient*(i-1) + offset-1.0)),0),ncolors)
//     blue[i]  = Math.min(Math.max(Math.floor(ncolors*(gradient*(i-1) + offset-2.0)),0),ncolors)
// }



