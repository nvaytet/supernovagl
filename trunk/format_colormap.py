from pylab import *

ff = open('colormap.js','w')

ncolors = 256

map_list = ["jet","hot","gnuplot","RdBu_r","RdGy_r","Greys","Greens"]

ff = open('colormap.js','w')
ff.write("function updateColormap() {\n")
ff.write("    ncolors = %i;\n" % (ncolors))
ff.write("    red   = [];\n")
ff.write("    green = [];\n")
ff.write("    blue  = [];\n")
ff.write("    red   = new Array(%i);\n" % (ncolors))
ff.write("    green = new Array(%i);\n" % (ncolors))
ff.write("    blue  = new Array(%i);\n" % (ncolors))

ff.write("    mymap = document.getElementById(\"selectColormap\").value;\n")

ff.write("    switch(mymap) {\n")


nmaps = shape(map_list)[0]

cNorm = matplotlib.colors.Normalize(vmin=0,vmax=ncolors-1)
    
for n in range(nmaps):
    
    thisMap = cm.get_cmap(map_list[n])
    scalarMap = matplotlib.cm.ScalarMappable(norm=cNorm,cmap=thisMap)
    
    ff.write("    case \""+map_list[n]+"\" :\n")
    
    for i in range(ncolors):
        color4 = scalarMap.to_rgba(i) #*(ncolors-1)
        #print color4[0]*(ncolors-1)
        ff.write("        red[%3i] = %3i; green[%3i] = %3i; blue[%3i] = %3i;\n"  % (i,round(color4[0]*(ncolors-1)),i,round(color4[1]*(ncolors-1)),i,round(color4[2]*(ncolors-1))))
    
    ff.write("        break;\n")
    

ff.write("    }\n")
ff.write("};\n")

ff.close



