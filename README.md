![Explosion](https://bitbucket.org/nvaytet/supernovagl/downloads/supernovagl.png)
![Gravity](https://bitbucket.org/nvaytet/supernovagl/downloads/Screenshot-11.png)

Download the archive, open the 'supernovagl.html' file in a browser and start blowing stuff up!

Or go to the online demo here: http://www.nbi.dk/~nvaytet/public_outreach/supernovagl.html

Left click for supernova explosion, middle click for black hole gravity field.